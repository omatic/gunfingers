<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.2.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCopper" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="253" name="Extra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="OmaticEagleParts">
<packages>
<package name="WS2812">
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.5" x2="2.5" y2="-1.5" width="0.127" layer="21"/>
<circle x="2.934" y="-2.688" radius="0.1414" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.1" width="0.127" layer="21"/>
<smd name="NC" x="-2" y="1.7" dx="2" dy="1.1" layer="1"/>
<smd name="LEDVDD" x="-2" y="0" dx="2" dy="1.1" layer="1"/>
<smd name="VSS" x="-2" y="-1.7" dx="2" dy="1.1" layer="1"/>
<smd name="DO" x="2" y="-1.7" dx="2" dy="1.1" layer="1"/>
<smd name="DI" x="2" y="0" dx="2" dy="1.1" layer="1"/>
<smd name="VDD" x="2" y="1.7" dx="2" dy="1.1" layer="1"/>
<text x="-1.7" y="2.9" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2" y="-4.1" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<pad name="VCC" x="0" y="0.635" drill="0.5" diameter="0.8128" stop="no"/>
<wire x1="1.27" y1="1.5875" x2="0.3175" y2="1.5875" width="0.6096" layer="1"/>
<wire x1="0.3175" y1="1.5875" x2="0" y2="1.27" width="0.6096" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="0.3175" width="0.6096" layer="1"/>
<wire x1="0" y1="0.3175" x2="-0.3175" y2="0" width="0.6096" layer="1"/>
<wire x1="-0.3175" y1="0" x2="-1.27" y2="0" width="0.6096" layer="1"/>
<pad name="DIN" x="0" y="-1.27" drill="0.5" diameter="0.8128" rot="R90" stop="no"/>
<pad name="DOUT" x="0" y="-2.54" drill="0.5" diameter="0.8128" rot="R90" stop="no"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.6096" layer="1"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.6096" layer="1"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-1.905" width="0.6096" layer="1"/>
</package>
<package name="BUTTONPAD-4X4">
<wire x1="6.7" y1="42.5" x2="8.2" y2="41" width="0.254" layer="1"/>
<wire x1="8.35" y1="39.88" x2="6.43" y2="41.16" width="0.254" layer="1"/>
<wire x1="5.36" y1="40.54" x2="7.42" y2="39.5" width="0.254" layer="1"/>
<wire x1="4.72" y1="38.01" x2="7.02" y2="37.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="36.89" x2="5.44" y2="36.75" width="0.254" layer="1"/>
<wire x1="5.04" y1="35.52" x2="7.31" y2="36.03" width="0.254" layer="1"/>
<wire x1="8.19" y1="35.42" x2="6.19" y2="34.48" width="0.254" layer="1"/>
<wire x1="6.13" y1="33.25" x2="8.03" y2="34.37" width="0.254" layer="1"/>
<wire x1="9.13" y1="34.13" x2="7.28" y2="32.58" width="0.254" layer="1"/>
<wire x1="8.11" y1="31.33" x2="9.44" y2="33.13" width="0.254" layer="1"/>
<wire x1="10.46" y1="33.28" x2="9.47" y2="31.19" width="0.254" layer="1"/>
<wire x1="10.17" y1="30.42" x2="10.91" y2="32.55" width="0.254" layer="1"/>
<wire x1="11.83" y1="32.93" x2="11.49" y2="30.66" width="0.254" layer="1"/>
<wire x1="12.42" y1="30.06" x2="12.49" y2="32.29" width="0.254" layer="1"/>
<wire x1="13.19" y1="33.04" x2="13.64" y2="30.74" width="0.254" layer="1"/>
<wire x1="14.79" y1="30.69" x2="14.05" y2="32.62" width="0.254" layer="1"/>
<wire x1="14.61" y1="33.6" x2="15.83" y2="31.51" width="0.254" layer="1"/>
<wire x1="16.74" y1="31.7" x2="15.57" y2="33.44" width="0.254" layer="1"/>
<wire x1="15.7" y1="34.56" x2="17.21" y2="32.98" width="0.254" layer="1"/>
<wire x1="18.37" y1="33.44" x2="16.64" y2="34.72" width="0.254" layer="1"/>
<wire x1="16.5" y1="35.9" x2="18.56" y2="34.88" width="0.254" layer="1"/>
<wire x1="19.42" y1="35.68" x2="17.25" y2="36.38" width="0.254" layer="1"/>
<wire x1="16.8" y1="37.07" x2="19.07" y2="36.91" width="0.254" layer="1"/>
<wire x1="19.58" y1="37.74" x2="17.38" y2="37.77" width="0.254" layer="1"/>
<wire x1="16.61" y1="38.36" x2="19.01" y2="38.68" width="0.254" layer="1"/>
<wire x1="19.31" y1="39.74" x2="17.09" y2="39.32" width="0.254" layer="1"/>
<wire x1="16.18" y1="39.66" x2="18.4" y2="40.65" width="0.254" layer="1"/>
<wire x1="18.35" y1="41.62" x2="16.34" y2="40.55" width="0.254" layer="1"/>
<wire x1="15.38" y1="40.76" x2="17.21" y2="42.2" width="0.254" layer="1"/>
<wire x1="16.72" y1="43.41" x2="15.17" y2="41.64" width="0.254" layer="1"/>
<wire x1="14.18" y1="41.59" x2="15.54" y2="43.51" width="0.254" layer="1"/>
<wire x1="14.53" y1="44.53" x2="13.7" y2="42.52" width="0.254" layer="1"/>
<wire x1="12.79" y1="42.12" x2="13.3" y2="44.29" width="0.254" layer="1"/>
<wire x1="9.29" y1="41.05" x2="7.76" y2="42.74" width="0.254" layer="1"/>
<wire x1="8.19" y1="43.7" x2="9.61" y2="41.86" width="0.254" layer="1"/>
<wire x1="10.46" y1="41.8" x2="9.35" y2="43.78" width="0.254" layer="1"/>
<wire x1="10.04" y1="44.61" x2="10.88" y2="42.68" width="0.254" layer="1"/>
<wire x1="11.59" y1="42.07" x2="11.21" y2="44.45" width="0.254" layer="1"/>
<wire x1="12.2" y1="44.96" x2="12.2" y2="42.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="38.5" x2="5.41" y2="39.24" width="0.254" layer="1"/>
<wire x1="5.41" y1="39.24" x2="7.78" y2="38.48" width="0.254" layer="29"/>
<wire x1="4.73" y1="38.01" x2="7.02" y2="37.87" width="0.254" layer="29"/>
<wire x1="5.44" y1="36.75" x2="7.74" y2="36.9" width="0.254" layer="29"/>
<wire x1="7.31" y1="36.03" x2="5.03" y2="35.52" width="0.254" layer="29"/>
<wire x1="6.19" y1="34.48" x2="8.19" y2="35.42" width="0.254" layer="29"/>
<wire x1="6.08" y1="33.22" x2="8.03" y2="34.37" width="0.254" layer="29"/>
<wire x1="9.15" y1="34.16" x2="7.28" y2="32.58" width="0.254" layer="29"/>
<wire x1="9.44" y1="33.13" x2="8.1" y2="31.28" width="0.254" layer="29"/>
<wire x1="9.46" y1="31.19" x2="10.45" y2="33.25" width="0.254" layer="29"/>
<wire x1="10.91" y1="32.55" x2="10.15" y2="30.37" width="0.254" layer="29"/>
<wire x1="11.49" y1="30.66" x2="11.84" y2="32.99" width="0.254" layer="29"/>
<wire x1="12.49" y1="32.29" x2="12.42" y2="30.06" width="0.254" layer="29"/>
<wire x1="13.64" y1="30.74" x2="13.18" y2="33.09" width="0.254" layer="29"/>
<wire x1="14.05" y1="32.62" x2="14.83" y2="30.57" width="0.254" layer="29"/>
<wire x1="15.83" y1="31.52" x2="14.58" y2="33.66" width="0.254" layer="29"/>
<wire x1="15.57" y1="33.44" x2="16.77" y2="31.66" width="0.254" layer="29"/>
<wire x1="15.66" y1="34.6" x2="17.21" y2="32.98" width="0.254" layer="29"/>
<wire x1="16.64" y1="34.72" x2="18.39" y2="33.42" width="0.254" layer="29"/>
<wire x1="16.43" y1="35.93" x2="18.56" y2="34.88" width="0.254" layer="29"/>
<wire x1="17.25" y1="36.38" x2="19.37" y2="35.7" width="0.254" layer="29"/>
<wire x1="16.71" y1="37.07" x2="19.07" y2="36.91" width="0.254" layer="29"/>
<wire x1="17.37" y1="37.77" x2="19.66" y2="37.74" width="0.254" layer="29"/>
<wire x1="16.67" y1="38.36" x2="19.01" y2="38.68" width="0.254" layer="29"/>
<wire x1="17.09" y1="39.32" x2="19.33" y2="39.74" width="0.254" layer="29"/>
<wire x1="16.23" y1="39.68" x2="18.4" y2="40.65" width="0.254" layer="29"/>
<wire x1="16.34" y1="40.55" x2="18.37" y2="41.64" width="0.254" layer="29"/>
<wire x1="15.39" y1="40.77" x2="17.21" y2="42.2" width="0.254" layer="29"/>
<wire x1="16.73" y1="43.42" x2="15.17" y2="41.64" width="0.254" layer="29"/>
<wire x1="14.2" y1="41.6" x2="15.54" y2="43.51" width="0.254" layer="29"/>
<wire x1="13.7" y1="42.52" x2="14.54" y2="44.54" width="0.254" layer="29"/>
<wire x1="12.76" y1="42.01" x2="13.3" y2="44.29" width="0.254" layer="29"/>
<wire x1="12.2" y1="42.87" x2="12.2" y2="44.99" width="0.254" layer="29"/>
<wire x1="11.62" y1="41.98" x2="11.21" y2="44.45" width="0.254" layer="29"/>
<wire x1="10.88" y1="42.68" x2="10.02" y2="44.65" width="0.254" layer="29"/>
<wire x1="9.35" y1="43.78" x2="10.46" y2="41.8" width="0.254" layer="29"/>
<wire x1="8.16" y1="43.74" x2="9.61" y2="41.86" width="0.254" layer="29"/>
<wire x1="7.76" y1="42.74" x2="9.32" y2="41.02" width="0.254" layer="29"/>
<wire x1="6.68" y1="42.51" x2="8.2" y2="41" width="0.254" layer="29"/>
<wire x1="6.43" y1="41.16" x2="8.29" y2="39.93" width="0.254" layer="29"/>
<wire x1="5.36" y1="40.53" x2="7.42" y2="39.5" width="0.254" layer="29"/>
<wire x1="31.7" y1="42.5" x2="33.2" y2="41" width="0.254" layer="1"/>
<wire x1="33.35" y1="39.88" x2="31.43" y2="41.16" width="0.254" layer="1"/>
<wire x1="30.36" y1="40.54" x2="32.42" y2="39.5" width="0.254" layer="1"/>
<wire x1="29.72" y1="38.01" x2="32.02" y2="37.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="36.89" x2="30.44" y2="36.75" width="0.254" layer="1"/>
<wire x1="30.04" y1="35.52" x2="32.31" y2="36.03" width="0.254" layer="1"/>
<wire x1="33.19" y1="35.42" x2="31.19" y2="34.48" width="0.254" layer="1"/>
<wire x1="31.13" y1="33.25" x2="33.03" y2="34.37" width="0.254" layer="1"/>
<wire x1="34.13" y1="34.13" x2="32.28" y2="32.58" width="0.254" layer="1"/>
<wire x1="33.11" y1="31.33" x2="34.44" y2="33.13" width="0.254" layer="1"/>
<wire x1="35.46" y1="33.28" x2="34.47" y2="31.19" width="0.254" layer="1"/>
<wire x1="35.17" y1="30.42" x2="35.91" y2="32.55" width="0.254" layer="1"/>
<wire x1="36.83" y1="32.93" x2="36.49" y2="30.66" width="0.254" layer="1"/>
<wire x1="37.42" y1="30.06" x2="37.49" y2="32.29" width="0.254" layer="1"/>
<wire x1="38.19" y1="33.04" x2="38.64" y2="30.74" width="0.254" layer="1"/>
<wire x1="39.79" y1="30.69" x2="39.05" y2="32.62" width="0.254" layer="1"/>
<wire x1="39.61" y1="33.6" x2="40.83" y2="31.51" width="0.254" layer="1"/>
<wire x1="41.74" y1="31.7" x2="40.57" y2="33.44" width="0.254" layer="1"/>
<wire x1="40.7" y1="34.56" x2="42.21" y2="32.98" width="0.254" layer="1"/>
<wire x1="43.37" y1="33.44" x2="41.64" y2="34.72" width="0.254" layer="1"/>
<wire x1="41.5" y1="35.9" x2="43.56" y2="34.88" width="0.254" layer="1"/>
<wire x1="44.42" y1="35.68" x2="42.25" y2="36.38" width="0.254" layer="1"/>
<wire x1="41.8" y1="37.07" x2="44.07" y2="36.91" width="0.254" layer="1"/>
<wire x1="44.58" y1="37.74" x2="42.38" y2="37.77" width="0.254" layer="1"/>
<wire x1="41.61" y1="38.36" x2="44.01" y2="38.68" width="0.254" layer="1"/>
<wire x1="44.31" y1="39.74" x2="42.09" y2="39.32" width="0.254" layer="1"/>
<wire x1="41.18" y1="39.66" x2="43.4" y2="40.65" width="0.254" layer="1"/>
<wire x1="43.35" y1="41.62" x2="41.34" y2="40.55" width="0.254" layer="1"/>
<wire x1="40.38" y1="40.76" x2="42.21" y2="42.2" width="0.254" layer="1"/>
<wire x1="41.72" y1="43.41" x2="40.17" y2="41.64" width="0.254" layer="1"/>
<wire x1="39.18" y1="41.59" x2="40.54" y2="43.51" width="0.254" layer="1"/>
<wire x1="39.53" y1="44.53" x2="38.7" y2="42.52" width="0.254" layer="1"/>
<wire x1="37.79" y1="42.12" x2="38.3" y2="44.29" width="0.254" layer="1"/>
<wire x1="34.29" y1="41.05" x2="32.76" y2="42.74" width="0.254" layer="1"/>
<wire x1="33.19" y1="43.7" x2="34.61" y2="41.86" width="0.254" layer="1"/>
<wire x1="35.46" y1="41.8" x2="34.35" y2="43.78" width="0.254" layer="1"/>
<wire x1="35.04" y1="44.61" x2="35.88" y2="42.68" width="0.254" layer="1"/>
<wire x1="36.59" y1="42.07" x2="36.21" y2="44.45" width="0.254" layer="1"/>
<wire x1="37.2" y1="44.96" x2="37.2" y2="42.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="38.5" x2="30.41" y2="39.24" width="0.254" layer="1"/>
<wire x1="30.41" y1="39.24" x2="32.78" y2="38.48" width="0.254" layer="29"/>
<wire x1="29.73" y1="38.01" x2="32.02" y2="37.87" width="0.254" layer="29"/>
<wire x1="30.44" y1="36.75" x2="32.74" y2="36.9" width="0.254" layer="29"/>
<wire x1="32.31" y1="36.03" x2="30.03" y2="35.52" width="0.254" layer="29"/>
<wire x1="31.19" y1="34.48" x2="33.19" y2="35.42" width="0.254" layer="29"/>
<wire x1="31.08" y1="33.22" x2="33.03" y2="34.37" width="0.254" layer="29"/>
<wire x1="34.15" y1="34.16" x2="32.28" y2="32.58" width="0.254" layer="29"/>
<wire x1="34.44" y1="33.13" x2="33.1" y2="31.28" width="0.254" layer="29"/>
<wire x1="34.46" y1="31.19" x2="35.45" y2="33.25" width="0.254" layer="29"/>
<wire x1="35.91" y1="32.55" x2="35.15" y2="30.37" width="0.254" layer="29"/>
<wire x1="36.49" y1="30.66" x2="36.84" y2="32.99" width="0.254" layer="29"/>
<wire x1="37.49" y1="32.29" x2="37.42" y2="30.06" width="0.254" layer="29"/>
<wire x1="38.64" y1="30.74" x2="38.18" y2="33.09" width="0.254" layer="29"/>
<wire x1="39.05" y1="32.62" x2="39.83" y2="30.57" width="0.254" layer="29"/>
<wire x1="40.83" y1="31.52" x2="39.58" y2="33.66" width="0.254" layer="29"/>
<wire x1="40.57" y1="33.44" x2="41.77" y2="31.66" width="0.254" layer="29"/>
<wire x1="40.66" y1="34.6" x2="42.21" y2="32.98" width="0.254" layer="29"/>
<wire x1="41.64" y1="34.72" x2="43.39" y2="33.42" width="0.254" layer="29"/>
<wire x1="41.43" y1="35.93" x2="43.56" y2="34.88" width="0.254" layer="29"/>
<wire x1="42.25" y1="36.38" x2="44.37" y2="35.7" width="0.254" layer="29"/>
<wire x1="41.71" y1="37.07" x2="44.07" y2="36.91" width="0.254" layer="29"/>
<wire x1="42.37" y1="37.77" x2="44.66" y2="37.74" width="0.254" layer="29"/>
<wire x1="41.67" y1="38.36" x2="44.01" y2="38.68" width="0.254" layer="29"/>
<wire x1="42.09" y1="39.32" x2="44.33" y2="39.74" width="0.254" layer="29"/>
<wire x1="41.23" y1="39.68" x2="43.4" y2="40.65" width="0.254" layer="29"/>
<wire x1="41.34" y1="40.55" x2="43.37" y2="41.64" width="0.254" layer="29"/>
<wire x1="40.39" y1="40.77" x2="42.21" y2="42.2" width="0.254" layer="29"/>
<wire x1="41.73" y1="43.42" x2="40.17" y2="41.64" width="0.254" layer="29"/>
<wire x1="39.2" y1="41.6" x2="40.54" y2="43.51" width="0.254" layer="29"/>
<wire x1="38.7" y1="42.52" x2="39.54" y2="44.54" width="0.254" layer="29"/>
<wire x1="37.76" y1="42.01" x2="38.3" y2="44.29" width="0.254" layer="29"/>
<wire x1="37.2" y1="42.87" x2="37.2" y2="44.99" width="0.254" layer="29"/>
<wire x1="36.62" y1="41.98" x2="36.21" y2="44.45" width="0.254" layer="29"/>
<wire x1="35.88" y1="42.68" x2="35.02" y2="44.65" width="0.254" layer="29"/>
<wire x1="34.35" y1="43.78" x2="35.46" y2="41.8" width="0.254" layer="29"/>
<wire x1="33.16" y1="43.74" x2="34.61" y2="41.86" width="0.254" layer="29"/>
<wire x1="32.76" y1="42.74" x2="34.32" y2="41.02" width="0.254" layer="29"/>
<wire x1="31.68" y1="42.51" x2="33.2" y2="41" width="0.254" layer="29"/>
<wire x1="31.43" y1="41.16" x2="33.29" y2="39.93" width="0.254" layer="29"/>
<wire x1="30.36" y1="40.53" x2="32.42" y2="39.5" width="0.254" layer="29"/>
<wire x1="6.7" y1="17.5" x2="8.2" y2="16" width="0.254" layer="1"/>
<wire x1="8.35" y1="14.88" x2="6.43" y2="16.16" width="0.254" layer="1"/>
<wire x1="5.36" y1="15.54" x2="7.42" y2="14.5" width="0.254" layer="1"/>
<wire x1="4.72" y1="13.01" x2="7.02" y2="12.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="11.89" x2="5.44" y2="11.75" width="0.254" layer="1"/>
<wire x1="5.04" y1="10.52" x2="7.31" y2="11.03" width="0.254" layer="1"/>
<wire x1="8.19" y1="10.42" x2="6.19" y2="9.48" width="0.254" layer="1"/>
<wire x1="6.13" y1="8.25" x2="8.03" y2="9.37" width="0.254" layer="1"/>
<wire x1="9.13" y1="9.13" x2="7.28" y2="7.58" width="0.254" layer="1"/>
<wire x1="8.11" y1="6.33" x2="9.44" y2="8.13" width="0.254" layer="1"/>
<wire x1="10.46" y1="8.28" x2="9.47" y2="6.19" width="0.254" layer="1"/>
<wire x1="10.17" y1="5.42" x2="10.91" y2="7.55" width="0.254" layer="1"/>
<wire x1="11.83" y1="7.93" x2="11.49" y2="5.66" width="0.254" layer="1"/>
<wire x1="12.42" y1="5.06" x2="12.49" y2="7.29" width="0.254" layer="1"/>
<wire x1="13.19" y1="8.04" x2="13.64" y2="5.74" width="0.254" layer="1"/>
<wire x1="14.79" y1="5.69" x2="14.05" y2="7.62" width="0.254" layer="1"/>
<wire x1="14.61" y1="8.6" x2="15.83" y2="6.51" width="0.254" layer="1"/>
<wire x1="16.74" y1="6.7" x2="15.57" y2="8.44" width="0.254" layer="1"/>
<wire x1="15.7" y1="9.56" x2="17.21" y2="7.98" width="0.254" layer="1"/>
<wire x1="18.37" y1="8.44" x2="16.64" y2="9.72" width="0.254" layer="1"/>
<wire x1="16.5" y1="10.9" x2="18.56" y2="9.88" width="0.254" layer="1"/>
<wire x1="19.42" y1="10.68" x2="17.25" y2="11.38" width="0.254" layer="1"/>
<wire x1="16.8" y1="12.07" x2="19.07" y2="11.91" width="0.254" layer="1"/>
<wire x1="19.58" y1="12.74" x2="17.38" y2="12.77" width="0.254" layer="1"/>
<wire x1="16.61" y1="13.36" x2="19.01" y2="13.68" width="0.254" layer="1"/>
<wire x1="19.31" y1="14.74" x2="17.09" y2="14.32" width="0.254" layer="1"/>
<wire x1="16.18" y1="14.66" x2="18.4" y2="15.65" width="0.254" layer="1"/>
<wire x1="18.35" y1="16.62" x2="16.34" y2="15.55" width="0.254" layer="1"/>
<wire x1="15.38" y1="15.76" x2="17.21" y2="17.2" width="0.254" layer="1"/>
<wire x1="16.72" y1="18.41" x2="15.17" y2="16.64" width="0.254" layer="1"/>
<wire x1="14.18" y1="16.59" x2="15.54" y2="18.51" width="0.254" layer="1"/>
<wire x1="14.53" y1="19.53" x2="13.7" y2="17.52" width="0.254" layer="1"/>
<wire x1="12.79" y1="17.12" x2="13.3" y2="19.29" width="0.254" layer="1"/>
<wire x1="9.29" y1="16.05" x2="7.76" y2="17.74" width="0.254" layer="1"/>
<wire x1="8.19" y1="18.7" x2="9.61" y2="16.86" width="0.254" layer="1"/>
<wire x1="10.46" y1="16.8" x2="9.35" y2="18.78" width="0.254" layer="1"/>
<wire x1="10.04" y1="19.61" x2="10.88" y2="17.68" width="0.254" layer="1"/>
<wire x1="11.59" y1="17.07" x2="11.21" y2="19.45" width="0.254" layer="1"/>
<wire x1="12.2" y1="19.96" x2="12.2" y2="17.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="13.5" x2="5.41" y2="14.24" width="0.254" layer="1"/>
<wire x1="5.41" y1="14.24" x2="7.78" y2="13.48" width="0.254" layer="29"/>
<wire x1="4.73" y1="13.01" x2="7.02" y2="12.87" width="0.254" layer="29"/>
<wire x1="5.44" y1="11.75" x2="7.74" y2="11.9" width="0.254" layer="29"/>
<wire x1="7.31" y1="11.03" x2="5.03" y2="10.52" width="0.254" layer="29"/>
<wire x1="6.19" y1="9.48" x2="8.19" y2="10.42" width="0.254" layer="29"/>
<wire x1="6.08" y1="8.22" x2="8.03" y2="9.37" width="0.254" layer="29"/>
<wire x1="9.15" y1="9.16" x2="7.28" y2="7.58" width="0.254" layer="29"/>
<wire x1="9.44" y1="8.13" x2="8.1" y2="6.28" width="0.254" layer="29"/>
<wire x1="9.46" y1="6.19" x2="10.45" y2="8.25" width="0.254" layer="29"/>
<wire x1="10.91" y1="7.55" x2="10.15" y2="5.37" width="0.254" layer="29"/>
<wire x1="11.49" y1="5.66" x2="11.84" y2="7.99" width="0.254" layer="29"/>
<wire x1="12.49" y1="7.29" x2="12.42" y2="5.06" width="0.254" layer="29"/>
<wire x1="13.64" y1="5.74" x2="13.18" y2="8.09" width="0.254" layer="29"/>
<wire x1="14.05" y1="7.62" x2="14.83" y2="5.57" width="0.254" layer="29"/>
<wire x1="15.83" y1="6.52" x2="14.58" y2="8.66" width="0.254" layer="29"/>
<wire x1="15.57" y1="8.44" x2="16.77" y2="6.66" width="0.254" layer="29"/>
<wire x1="15.66" y1="9.6" x2="17.21" y2="7.98" width="0.254" layer="29"/>
<wire x1="16.64" y1="9.72" x2="18.39" y2="8.42" width="0.254" layer="29"/>
<wire x1="16.43" y1="10.93" x2="18.56" y2="9.88" width="0.254" layer="29"/>
<wire x1="17.25" y1="11.38" x2="19.37" y2="10.7" width="0.254" layer="29"/>
<wire x1="16.71" y1="12.07" x2="19.07" y2="11.91" width="0.254" layer="29"/>
<wire x1="17.37" y1="12.77" x2="19.66" y2="12.74" width="0.254" layer="29"/>
<wire x1="16.67" y1="13.36" x2="19.01" y2="13.68" width="0.254" layer="29"/>
<wire x1="17.09" y1="14.32" x2="19.33" y2="14.74" width="0.254" layer="29"/>
<wire x1="16.23" y1="14.68" x2="18.4" y2="15.65" width="0.254" layer="29"/>
<wire x1="16.34" y1="15.55" x2="18.37" y2="16.64" width="0.254" layer="29"/>
<wire x1="15.39" y1="15.77" x2="17.21" y2="17.2" width="0.254" layer="29"/>
<wire x1="16.73" y1="18.42" x2="15.17" y2="16.64" width="0.254" layer="29"/>
<wire x1="14.2" y1="16.6" x2="15.54" y2="18.51" width="0.254" layer="29"/>
<wire x1="13.7" y1="17.52" x2="14.54" y2="19.54" width="0.254" layer="29"/>
<wire x1="12.76" y1="17.01" x2="13.3" y2="19.29" width="0.254" layer="29"/>
<wire x1="12.2" y1="17.87" x2="12.2" y2="19.99" width="0.254" layer="29"/>
<wire x1="11.62" y1="16.98" x2="11.21" y2="19.45" width="0.254" layer="29"/>
<wire x1="10.88" y1="17.68" x2="10.02" y2="19.65" width="0.254" layer="29"/>
<wire x1="9.35" y1="18.78" x2="10.46" y2="16.8" width="0.254" layer="29"/>
<wire x1="8.16" y1="18.74" x2="9.61" y2="16.86" width="0.254" layer="29"/>
<wire x1="7.76" y1="17.74" x2="9.32" y2="16.02" width="0.254" layer="29"/>
<wire x1="6.68" y1="17.51" x2="8.2" y2="16" width="0.254" layer="29"/>
<wire x1="6.43" y1="16.16" x2="8.29" y2="14.93" width="0.254" layer="29"/>
<wire x1="5.36" y1="15.53" x2="7.42" y2="14.5" width="0.254" layer="29"/>
<wire x1="31.7" y1="17.5" x2="33.2" y2="16" width="0.254" layer="1"/>
<wire x1="33.35" y1="14.88" x2="31.43" y2="16.16" width="0.254" layer="1"/>
<wire x1="30.36" y1="15.54" x2="32.42" y2="14.5" width="0.254" layer="1"/>
<wire x1="29.72" y1="13.01" x2="32.02" y2="12.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="11.89" x2="30.44" y2="11.75" width="0.254" layer="1"/>
<wire x1="30.04" y1="10.52" x2="32.31" y2="11.03" width="0.254" layer="1"/>
<wire x1="33.19" y1="10.42" x2="31.19" y2="9.48" width="0.254" layer="1"/>
<wire x1="31.13" y1="8.25" x2="33.03" y2="9.37" width="0.254" layer="1"/>
<wire x1="34.13" y1="9.13" x2="32.28" y2="7.58" width="0.254" layer="1"/>
<wire x1="33.11" y1="6.33" x2="34.44" y2="8.13" width="0.254" layer="1"/>
<wire x1="35.46" y1="8.28" x2="34.47" y2="6.19" width="0.254" layer="1"/>
<wire x1="35.17" y1="5.42" x2="35.91" y2="7.55" width="0.254" layer="1"/>
<wire x1="36.83" y1="7.93" x2="36.49" y2="5.66" width="0.254" layer="1"/>
<wire x1="37.42" y1="5.06" x2="37.49" y2="7.29" width="0.254" layer="1"/>
<wire x1="38.19" y1="8.04" x2="38.64" y2="5.74" width="0.254" layer="1"/>
<wire x1="39.79" y1="5.69" x2="39.05" y2="7.62" width="0.254" layer="1"/>
<wire x1="39.61" y1="8.6" x2="40.83" y2="6.51" width="0.254" layer="1"/>
<wire x1="41.74" y1="6.7" x2="40.57" y2="8.44" width="0.254" layer="1"/>
<wire x1="40.7" y1="9.56" x2="42.21" y2="7.98" width="0.254" layer="1"/>
<wire x1="43.37" y1="8.44" x2="41.64" y2="9.72" width="0.254" layer="1"/>
<wire x1="41.5" y1="10.9" x2="43.56" y2="9.88" width="0.254" layer="1"/>
<wire x1="44.42" y1="10.68" x2="42.25" y2="11.38" width="0.254" layer="1"/>
<wire x1="41.8" y1="12.07" x2="44.07" y2="11.91" width="0.254" layer="1"/>
<wire x1="44.58" y1="12.74" x2="42.38" y2="12.77" width="0.254" layer="1"/>
<wire x1="41.61" y1="13.36" x2="44.01" y2="13.68" width="0.254" layer="1"/>
<wire x1="44.31" y1="14.74" x2="42.09" y2="14.32" width="0.254" layer="1"/>
<wire x1="41.18" y1="14.66" x2="43.4" y2="15.65" width="0.254" layer="1"/>
<wire x1="43.35" y1="16.62" x2="41.34" y2="15.55" width="0.254" layer="1"/>
<wire x1="40.38" y1="15.76" x2="42.21" y2="17.2" width="0.254" layer="1"/>
<wire x1="41.72" y1="18.41" x2="40.17" y2="16.64" width="0.254" layer="1"/>
<wire x1="39.18" y1="16.59" x2="40.54" y2="18.51" width="0.254" layer="1"/>
<wire x1="39.53" y1="19.53" x2="38.7" y2="17.52" width="0.254" layer="1"/>
<wire x1="37.79" y1="17.12" x2="38.3" y2="19.29" width="0.254" layer="1"/>
<wire x1="34.29" y1="16.05" x2="32.76" y2="17.74" width="0.254" layer="1"/>
<wire x1="33.19" y1="18.7" x2="34.61" y2="16.86" width="0.254" layer="1"/>
<wire x1="35.46" y1="16.8" x2="34.35" y2="18.78" width="0.254" layer="1"/>
<wire x1="35.04" y1="19.61" x2="35.88" y2="17.68" width="0.254" layer="1"/>
<wire x1="36.59" y1="17.07" x2="36.21" y2="19.45" width="0.254" layer="1"/>
<wire x1="37.2" y1="19.96" x2="37.2" y2="17.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="13.5" x2="30.41" y2="14.24" width="0.254" layer="1"/>
<wire x1="30.41" y1="14.24" x2="32.78" y2="13.48" width="0.254" layer="29"/>
<wire x1="29.73" y1="13.01" x2="32.02" y2="12.87" width="0.254" layer="29"/>
<wire x1="30.44" y1="11.75" x2="32.74" y2="11.9" width="0.254" layer="29"/>
<wire x1="32.31" y1="11.03" x2="30.03" y2="10.52" width="0.254" layer="29"/>
<wire x1="31.19" y1="9.48" x2="33.19" y2="10.42" width="0.254" layer="29"/>
<wire x1="31.08" y1="8.22" x2="33.03" y2="9.37" width="0.254" layer="29"/>
<wire x1="34.15" y1="9.16" x2="32.28" y2="7.58" width="0.254" layer="29"/>
<wire x1="34.44" y1="8.13" x2="33.1" y2="6.28" width="0.254" layer="29"/>
<wire x1="34.46" y1="6.19" x2="35.45" y2="8.25" width="0.254" layer="29"/>
<wire x1="35.91" y1="7.55" x2="35.15" y2="5.37" width="0.254" layer="29"/>
<wire x1="36.49" y1="5.66" x2="36.84" y2="7.99" width="0.254" layer="29"/>
<wire x1="37.49" y1="7.29" x2="37.42" y2="5.06" width="0.254" layer="29"/>
<wire x1="38.64" y1="5.74" x2="38.18" y2="8.09" width="0.254" layer="29"/>
<wire x1="39.05" y1="7.62" x2="39.83" y2="5.57" width="0.254" layer="29"/>
<wire x1="40.83" y1="6.52" x2="39.58" y2="8.66" width="0.254" layer="29"/>
<wire x1="40.57" y1="8.44" x2="41.77" y2="6.66" width="0.254" layer="29"/>
<wire x1="40.66" y1="9.6" x2="42.21" y2="7.98" width="0.254" layer="29"/>
<wire x1="41.64" y1="9.72" x2="43.39" y2="8.42" width="0.254" layer="29"/>
<wire x1="41.43" y1="10.93" x2="43.56" y2="9.88" width="0.254" layer="29"/>
<wire x1="42.25" y1="11.38" x2="44.37" y2="10.7" width="0.254" layer="29"/>
<wire x1="41.71" y1="12.07" x2="44.07" y2="11.91" width="0.254" layer="29"/>
<wire x1="42.37" y1="12.77" x2="44.66" y2="12.74" width="0.254" layer="29"/>
<wire x1="41.67" y1="13.36" x2="44.01" y2="13.68" width="0.254" layer="29"/>
<wire x1="42.09" y1="14.32" x2="44.33" y2="14.74" width="0.254" layer="29"/>
<wire x1="41.23" y1="14.68" x2="43.4" y2="15.65" width="0.254" layer="29"/>
<wire x1="41.34" y1="15.55" x2="43.37" y2="16.64" width="0.254" layer="29"/>
<wire x1="40.39" y1="15.77" x2="42.21" y2="17.2" width="0.254" layer="29"/>
<wire x1="41.73" y1="18.42" x2="40.17" y2="16.64" width="0.254" layer="29"/>
<wire x1="39.2" y1="16.6" x2="40.54" y2="18.51" width="0.254" layer="29"/>
<wire x1="38.7" y1="17.52" x2="39.54" y2="19.54" width="0.254" layer="29"/>
<wire x1="37.76" y1="17.01" x2="38.3" y2="19.29" width="0.254" layer="29"/>
<wire x1="37.2" y1="17.87" x2="37.2" y2="19.99" width="0.254" layer="29"/>
<wire x1="36.62" y1="16.98" x2="36.21" y2="19.45" width="0.254" layer="29"/>
<wire x1="35.88" y1="17.68" x2="35.02" y2="19.65" width="0.254" layer="29"/>
<wire x1="34.35" y1="18.78" x2="35.46" y2="16.8" width="0.254" layer="29"/>
<wire x1="33.16" y1="18.74" x2="34.61" y2="16.86" width="0.254" layer="29"/>
<wire x1="32.76" y1="17.74" x2="34.32" y2="16.02" width="0.254" layer="29"/>
<wire x1="31.68" y1="17.51" x2="33.2" y2="16" width="0.254" layer="29"/>
<wire x1="31.43" y1="16.16" x2="33.29" y2="14.93" width="0.254" layer="29"/>
<wire x1="30.36" y1="15.53" x2="32.42" y2="14.5" width="0.254" layer="29"/>
<wire x1="100" y1="0" x2="0" y2="0" width="0.254" layer="21"/>
<wire x1="56.7" y1="42.5" x2="58.2" y2="41" width="0.254" layer="1"/>
<wire x1="58.35" y1="39.88" x2="56.43" y2="41.16" width="0.254" layer="1"/>
<wire x1="55.36" y1="40.54" x2="57.42" y2="39.5" width="0.254" layer="1"/>
<wire x1="54.72" y1="38.01" x2="57.02" y2="37.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="36.89" x2="55.44" y2="36.75" width="0.254" layer="1"/>
<wire x1="55.04" y1="35.52" x2="57.31" y2="36.03" width="0.254" layer="1"/>
<wire x1="58.19" y1="35.42" x2="56.19" y2="34.48" width="0.254" layer="1"/>
<wire x1="56.13" y1="33.25" x2="58.03" y2="34.37" width="0.254" layer="1"/>
<wire x1="59.13" y1="34.13" x2="57.28" y2="32.58" width="0.254" layer="1"/>
<wire x1="58.11" y1="31.33" x2="59.44" y2="33.13" width="0.254" layer="1"/>
<wire x1="60.46" y1="33.28" x2="59.47" y2="31.19" width="0.254" layer="1"/>
<wire x1="60.17" y1="30.42" x2="60.91" y2="32.55" width="0.254" layer="1"/>
<wire x1="61.83" y1="32.93" x2="61.49" y2="30.66" width="0.254" layer="1"/>
<wire x1="62.42" y1="30.06" x2="62.49" y2="32.29" width="0.254" layer="1"/>
<wire x1="63.19" y1="33.04" x2="63.64" y2="30.74" width="0.254" layer="1"/>
<wire x1="64.79" y1="30.69" x2="64.05" y2="32.62" width="0.254" layer="1"/>
<wire x1="64.61" y1="33.6" x2="65.83" y2="31.51" width="0.254" layer="1"/>
<wire x1="66.74" y1="31.7" x2="65.57" y2="33.44" width="0.254" layer="1"/>
<wire x1="65.7" y1="34.56" x2="67.21" y2="32.98" width="0.254" layer="1"/>
<wire x1="68.37" y1="33.44" x2="66.64" y2="34.72" width="0.254" layer="1"/>
<wire x1="66.5" y1="35.9" x2="68.56" y2="34.88" width="0.254" layer="1"/>
<wire x1="69.42" y1="35.68" x2="67.25" y2="36.38" width="0.254" layer="1"/>
<wire x1="66.8" y1="37.07" x2="69.07" y2="36.91" width="0.254" layer="1"/>
<wire x1="69.58" y1="37.74" x2="67.38" y2="37.77" width="0.254" layer="1"/>
<wire x1="66.61" y1="38.36" x2="69.01" y2="38.68" width="0.254" layer="1"/>
<wire x1="69.31" y1="39.74" x2="67.09" y2="39.32" width="0.254" layer="1"/>
<wire x1="66.18" y1="39.66" x2="68.4" y2="40.65" width="0.254" layer="1"/>
<wire x1="68.35" y1="41.62" x2="66.34" y2="40.55" width="0.254" layer="1"/>
<wire x1="65.38" y1="40.76" x2="67.21" y2="42.2" width="0.254" layer="1"/>
<wire x1="66.72" y1="43.41" x2="65.17" y2="41.64" width="0.254" layer="1"/>
<wire x1="64.18" y1="41.59" x2="65.54" y2="43.51" width="0.254" layer="1"/>
<wire x1="64.53" y1="44.53" x2="63.7" y2="42.52" width="0.254" layer="1"/>
<wire x1="62.79" y1="42.12" x2="63.3" y2="44.29" width="0.254" layer="1"/>
<wire x1="59.29" y1="41.05" x2="57.76" y2="42.74" width="0.254" layer="1"/>
<wire x1="58.19" y1="43.7" x2="59.61" y2="41.86" width="0.254" layer="1"/>
<wire x1="60.46" y1="41.8" x2="59.35" y2="43.78" width="0.254" layer="1"/>
<wire x1="60.04" y1="44.61" x2="60.88" y2="42.68" width="0.254" layer="1"/>
<wire x1="61.59" y1="42.07" x2="61.21" y2="44.45" width="0.254" layer="1"/>
<wire x1="62.2" y1="44.96" x2="62.2" y2="42.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="38.5" x2="55.41" y2="39.24" width="0.254" layer="1"/>
<wire x1="55.41" y1="39.24" x2="57.78" y2="38.48" width="0.254" layer="29"/>
<wire x1="54.73" y1="38.01" x2="57.02" y2="37.87" width="0.254" layer="29"/>
<wire x1="55.44" y1="36.75" x2="57.74" y2="36.9" width="0.254" layer="29"/>
<wire x1="57.31" y1="36.03" x2="55.03" y2="35.52" width="0.254" layer="29"/>
<wire x1="56.19" y1="34.48" x2="58.19" y2="35.42" width="0.254" layer="29"/>
<wire x1="56.08" y1="33.22" x2="58.03" y2="34.37" width="0.254" layer="29"/>
<wire x1="59.15" y1="34.16" x2="57.28" y2="32.58" width="0.254" layer="29"/>
<wire x1="59.44" y1="33.13" x2="58.1" y2="31.28" width="0.254" layer="29"/>
<wire x1="59.46" y1="31.19" x2="60.45" y2="33.25" width="0.254" layer="29"/>
<wire x1="60.91" y1="32.55" x2="60.15" y2="30.37" width="0.254" layer="29"/>
<wire x1="61.49" y1="30.66" x2="61.84" y2="32.99" width="0.254" layer="29"/>
<wire x1="62.49" y1="32.29" x2="62.42" y2="30.06" width="0.254" layer="29"/>
<wire x1="63.64" y1="30.74" x2="63.18" y2="33.09" width="0.254" layer="29"/>
<wire x1="64.05" y1="32.62" x2="64.83" y2="30.57" width="0.254" layer="29"/>
<wire x1="65.83" y1="31.52" x2="64.58" y2="33.66" width="0.254" layer="29"/>
<wire x1="65.57" y1="33.44" x2="66.77" y2="31.66" width="0.254" layer="29"/>
<wire x1="65.66" y1="34.6" x2="67.21" y2="32.98" width="0.254" layer="29"/>
<wire x1="66.64" y1="34.72" x2="68.39" y2="33.42" width="0.254" layer="29"/>
<wire x1="66.43" y1="35.93" x2="68.56" y2="34.88" width="0.254" layer="29"/>
<wire x1="67.25" y1="36.38" x2="69.37" y2="35.7" width="0.254" layer="29"/>
<wire x1="66.71" y1="37.07" x2="69.07" y2="36.91" width="0.254" layer="29"/>
<wire x1="67.37" y1="37.77" x2="69.66" y2="37.74" width="0.254" layer="29"/>
<wire x1="66.67" y1="38.36" x2="69.01" y2="38.68" width="0.254" layer="29"/>
<wire x1="67.09" y1="39.32" x2="69.33" y2="39.74" width="0.254" layer="29"/>
<wire x1="66.23" y1="39.68" x2="68.4" y2="40.65" width="0.254" layer="29"/>
<wire x1="66.34" y1="40.55" x2="68.37" y2="41.64" width="0.254" layer="29"/>
<wire x1="65.39" y1="40.77" x2="67.21" y2="42.2" width="0.254" layer="29"/>
<wire x1="66.73" y1="43.42" x2="65.17" y2="41.64" width="0.254" layer="29"/>
<wire x1="64.2" y1="41.6" x2="65.54" y2="43.51" width="0.254" layer="29"/>
<wire x1="63.7" y1="42.52" x2="64.54" y2="44.54" width="0.254" layer="29"/>
<wire x1="62.76" y1="42.01" x2="63.3" y2="44.29" width="0.254" layer="29"/>
<wire x1="62.2" y1="42.87" x2="62.2" y2="44.99" width="0.254" layer="29"/>
<wire x1="61.62" y1="41.98" x2="61.21" y2="44.45" width="0.254" layer="29"/>
<wire x1="60.88" y1="42.68" x2="60.02" y2="44.65" width="0.254" layer="29"/>
<wire x1="59.35" y1="43.78" x2="60.46" y2="41.8" width="0.254" layer="29"/>
<wire x1="58.16" y1="43.74" x2="59.61" y2="41.86" width="0.254" layer="29"/>
<wire x1="57.76" y1="42.74" x2="59.32" y2="41.02" width="0.254" layer="29"/>
<wire x1="56.68" y1="42.51" x2="58.2" y2="41" width="0.254" layer="29"/>
<wire x1="56.43" y1="41.16" x2="58.29" y2="39.93" width="0.254" layer="29"/>
<wire x1="55.36" y1="40.53" x2="57.42" y2="39.5" width="0.254" layer="29"/>
<wire x1="81.7" y1="42.5" x2="83.2" y2="41" width="0.254" layer="1"/>
<wire x1="83.35" y1="39.88" x2="81.43" y2="41.16" width="0.254" layer="1"/>
<wire x1="80.36" y1="40.54" x2="82.42" y2="39.5" width="0.254" layer="1"/>
<wire x1="79.72" y1="38.01" x2="82.02" y2="37.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="36.89" x2="80.44" y2="36.75" width="0.254" layer="1"/>
<wire x1="80.04" y1="35.52" x2="82.31" y2="36.03" width="0.254" layer="1"/>
<wire x1="83.19" y1="35.42" x2="81.19" y2="34.48" width="0.254" layer="1"/>
<wire x1="81.13" y1="33.25" x2="83.03" y2="34.37" width="0.254" layer="1"/>
<wire x1="84.13" y1="34.13" x2="82.28" y2="32.58" width="0.254" layer="1"/>
<wire x1="83.11" y1="31.33" x2="84.44" y2="33.13" width="0.254" layer="1"/>
<wire x1="85.46" y1="33.28" x2="84.47" y2="31.19" width="0.254" layer="1"/>
<wire x1="85.17" y1="30.42" x2="85.91" y2="32.55" width="0.254" layer="1"/>
<wire x1="86.83" y1="32.93" x2="86.49" y2="30.66" width="0.254" layer="1"/>
<wire x1="87.42" y1="30.06" x2="87.49" y2="32.29" width="0.254" layer="1"/>
<wire x1="88.19" y1="33.04" x2="88.64" y2="30.74" width="0.254" layer="1"/>
<wire x1="89.79" y1="30.69" x2="89.05" y2="32.62" width="0.254" layer="1"/>
<wire x1="89.61" y1="33.6" x2="90.83" y2="31.51" width="0.254" layer="1"/>
<wire x1="91.74" y1="31.7" x2="90.57" y2="33.44" width="0.254" layer="1"/>
<wire x1="90.7" y1="34.56" x2="92.21" y2="32.98" width="0.254" layer="1"/>
<wire x1="93.37" y1="33.44" x2="91.64" y2="34.72" width="0.254" layer="1"/>
<wire x1="91.5" y1="35.9" x2="93.56" y2="34.88" width="0.254" layer="1"/>
<wire x1="94.42" y1="35.68" x2="92.25" y2="36.38" width="0.254" layer="1"/>
<wire x1="91.8" y1="37.07" x2="94.07" y2="36.91" width="0.254" layer="1"/>
<wire x1="94.58" y1="37.74" x2="92.38" y2="37.77" width="0.254" layer="1"/>
<wire x1="91.61" y1="38.36" x2="94.01" y2="38.68" width="0.254" layer="1"/>
<wire x1="94.31" y1="39.74" x2="92.09" y2="39.32" width="0.254" layer="1"/>
<wire x1="91.18" y1="39.66" x2="93.4" y2="40.65" width="0.254" layer="1"/>
<wire x1="93.35" y1="41.62" x2="91.34" y2="40.55" width="0.254" layer="1"/>
<wire x1="90.38" y1="40.76" x2="92.21" y2="42.2" width="0.254" layer="1"/>
<wire x1="91.72" y1="43.41" x2="90.17" y2="41.64" width="0.254" layer="1"/>
<wire x1="89.18" y1="41.59" x2="90.54" y2="43.51" width="0.254" layer="1"/>
<wire x1="89.53" y1="44.53" x2="88.7" y2="42.52" width="0.254" layer="1"/>
<wire x1="87.79" y1="42.12" x2="88.3" y2="44.29" width="0.254" layer="1"/>
<wire x1="84.29" y1="41.05" x2="82.76" y2="42.74" width="0.254" layer="1"/>
<wire x1="83.19" y1="43.7" x2="84.61" y2="41.86" width="0.254" layer="1"/>
<wire x1="85.46" y1="41.8" x2="84.35" y2="43.78" width="0.254" layer="1"/>
<wire x1="85.04" y1="44.61" x2="85.88" y2="42.68" width="0.254" layer="1"/>
<wire x1="86.59" y1="42.07" x2="86.21" y2="44.45" width="0.254" layer="1"/>
<wire x1="87.2" y1="44.96" x2="87.2" y2="42.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="38.5" x2="80.41" y2="39.24" width="0.254" layer="1"/>
<wire x1="80.41" y1="39.24" x2="82.78" y2="38.48" width="0.254" layer="29"/>
<wire x1="79.73" y1="38.01" x2="82.02" y2="37.87" width="0.254" layer="29"/>
<wire x1="80.44" y1="36.75" x2="82.74" y2="36.9" width="0.254" layer="29"/>
<wire x1="82.31" y1="36.03" x2="80.03" y2="35.52" width="0.254" layer="29"/>
<wire x1="81.19" y1="34.48" x2="83.19" y2="35.42" width="0.254" layer="29"/>
<wire x1="81.08" y1="33.22" x2="83.03" y2="34.37" width="0.254" layer="29"/>
<wire x1="84.15" y1="34.16" x2="82.28" y2="32.58" width="0.254" layer="29"/>
<wire x1="84.44" y1="33.13" x2="83.1" y2="31.28" width="0.254" layer="29"/>
<wire x1="84.46" y1="31.19" x2="85.45" y2="33.25" width="0.254" layer="29"/>
<wire x1="85.91" y1="32.55" x2="85.15" y2="30.37" width="0.254" layer="29"/>
<wire x1="86.49" y1="30.66" x2="86.84" y2="32.99" width="0.254" layer="29"/>
<wire x1="87.49" y1="32.29" x2="87.42" y2="30.06" width="0.254" layer="29"/>
<wire x1="88.64" y1="30.74" x2="88.18" y2="33.09" width="0.254" layer="29"/>
<wire x1="89.05" y1="32.62" x2="89.83" y2="30.57" width="0.254" layer="29"/>
<wire x1="90.83" y1="31.52" x2="89.58" y2="33.66" width="0.254" layer="29"/>
<wire x1="90.57" y1="33.44" x2="91.77" y2="31.66" width="0.254" layer="29"/>
<wire x1="90.66" y1="34.6" x2="92.21" y2="32.98" width="0.254" layer="29"/>
<wire x1="91.64" y1="34.72" x2="93.39" y2="33.42" width="0.254" layer="29"/>
<wire x1="91.43" y1="35.93" x2="93.56" y2="34.88" width="0.254" layer="29"/>
<wire x1="92.25" y1="36.38" x2="94.37" y2="35.7" width="0.254" layer="29"/>
<wire x1="91.71" y1="37.07" x2="94.07" y2="36.91" width="0.254" layer="29"/>
<wire x1="92.37" y1="37.77" x2="94.66" y2="37.74" width="0.254" layer="29"/>
<wire x1="91.67" y1="38.36" x2="94.01" y2="38.68" width="0.254" layer="29"/>
<wire x1="92.09" y1="39.32" x2="94.33" y2="39.74" width="0.254" layer="29"/>
<wire x1="91.23" y1="39.68" x2="93.4" y2="40.65" width="0.254" layer="29"/>
<wire x1="91.34" y1="40.55" x2="93.37" y2="41.64" width="0.254" layer="29"/>
<wire x1="90.39" y1="40.77" x2="92.21" y2="42.2" width="0.254" layer="29"/>
<wire x1="91.73" y1="43.42" x2="90.17" y2="41.64" width="0.254" layer="29"/>
<wire x1="89.2" y1="41.6" x2="90.54" y2="43.51" width="0.254" layer="29"/>
<wire x1="88.7" y1="42.52" x2="89.54" y2="44.54" width="0.254" layer="29"/>
<wire x1="87.76" y1="42.01" x2="88.3" y2="44.29" width="0.254" layer="29"/>
<wire x1="87.2" y1="42.87" x2="87.2" y2="44.99" width="0.254" layer="29"/>
<wire x1="86.62" y1="41.98" x2="86.21" y2="44.45" width="0.254" layer="29"/>
<wire x1="85.88" y1="42.68" x2="85.02" y2="44.65" width="0.254" layer="29"/>
<wire x1="84.35" y1="43.78" x2="85.46" y2="41.8" width="0.254" layer="29"/>
<wire x1="83.16" y1="43.74" x2="84.61" y2="41.86" width="0.254" layer="29"/>
<wire x1="82.76" y1="42.74" x2="84.32" y2="41.02" width="0.254" layer="29"/>
<wire x1="81.68" y1="42.51" x2="83.2" y2="41" width="0.254" layer="29"/>
<wire x1="81.43" y1="41.16" x2="83.29" y2="39.93" width="0.254" layer="29"/>
<wire x1="80.36" y1="40.53" x2="82.42" y2="39.5" width="0.254" layer="29"/>
<wire x1="56.7" y1="17.5" x2="58.2" y2="16" width="0.254" layer="1"/>
<wire x1="58.35" y1="14.88" x2="56.43" y2="16.16" width="0.254" layer="1"/>
<wire x1="55.36" y1="15.54" x2="57.42" y2="14.5" width="0.254" layer="1"/>
<wire x1="54.72" y1="13.01" x2="57.02" y2="12.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="11.89" x2="55.44" y2="11.75" width="0.254" layer="1"/>
<wire x1="55.04" y1="10.52" x2="57.31" y2="11.03" width="0.254" layer="1"/>
<wire x1="58.19" y1="10.42" x2="56.19" y2="9.48" width="0.254" layer="1"/>
<wire x1="56.13" y1="8.25" x2="58.03" y2="9.37" width="0.254" layer="1"/>
<wire x1="59.13" y1="9.13" x2="57.28" y2="7.58" width="0.254" layer="1"/>
<wire x1="58.11" y1="6.33" x2="59.44" y2="8.13" width="0.254" layer="1"/>
<wire x1="60.46" y1="8.28" x2="59.47" y2="6.19" width="0.254" layer="1"/>
<wire x1="60.17" y1="5.42" x2="60.91" y2="7.55" width="0.254" layer="1"/>
<wire x1="61.83" y1="7.93" x2="61.49" y2="5.66" width="0.254" layer="1"/>
<wire x1="62.42" y1="5.06" x2="62.49" y2="7.29" width="0.254" layer="1"/>
<wire x1="63.19" y1="8.04" x2="63.64" y2="5.74" width="0.254" layer="1"/>
<wire x1="64.79" y1="5.69" x2="64.05" y2="7.62" width="0.254" layer="1"/>
<wire x1="64.61" y1="8.6" x2="65.83" y2="6.51" width="0.254" layer="1"/>
<wire x1="66.74" y1="6.7" x2="65.57" y2="8.44" width="0.254" layer="1"/>
<wire x1="65.7" y1="9.56" x2="67.21" y2="7.98" width="0.254" layer="1"/>
<wire x1="68.37" y1="8.44" x2="66.64" y2="9.72" width="0.254" layer="1"/>
<wire x1="66.5" y1="10.9" x2="68.56" y2="9.88" width="0.254" layer="1"/>
<wire x1="69.42" y1="10.68" x2="67.25" y2="11.38" width="0.254" layer="1"/>
<wire x1="66.8" y1="12.07" x2="69.07" y2="11.91" width="0.254" layer="1"/>
<wire x1="69.58" y1="12.74" x2="67.38" y2="12.77" width="0.254" layer="1"/>
<wire x1="66.61" y1="13.36" x2="69.01" y2="13.68" width="0.254" layer="1"/>
<wire x1="69.31" y1="14.74" x2="67.09" y2="14.32" width="0.254" layer="1"/>
<wire x1="66.18" y1="14.66" x2="68.4" y2="15.65" width="0.254" layer="1"/>
<wire x1="68.35" y1="16.62" x2="66.34" y2="15.55" width="0.254" layer="1"/>
<wire x1="65.38" y1="15.76" x2="67.21" y2="17.2" width="0.254" layer="1"/>
<wire x1="66.72" y1="18.41" x2="65.17" y2="16.64" width="0.254" layer="1"/>
<wire x1="64.18" y1="16.59" x2="65.54" y2="18.51" width="0.254" layer="1"/>
<wire x1="64.53" y1="19.53" x2="63.7" y2="17.52" width="0.254" layer="1"/>
<wire x1="62.79" y1="17.12" x2="63.3" y2="19.29" width="0.254" layer="1"/>
<wire x1="59.29" y1="16.05" x2="57.76" y2="17.74" width="0.254" layer="1"/>
<wire x1="58.19" y1="18.7" x2="59.61" y2="16.86" width="0.254" layer="1"/>
<wire x1="60.46" y1="16.8" x2="59.35" y2="18.78" width="0.254" layer="1"/>
<wire x1="60.04" y1="19.61" x2="60.88" y2="17.68" width="0.254" layer="1"/>
<wire x1="61.59" y1="17.07" x2="61.21" y2="19.45" width="0.254" layer="1"/>
<wire x1="62.2" y1="19.96" x2="62.2" y2="17.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="13.5" x2="55.41" y2="14.24" width="0.254" layer="1"/>
<wire x1="55.41" y1="14.24" x2="57.78" y2="13.48" width="0.254" layer="29"/>
<wire x1="54.73" y1="13.01" x2="57.02" y2="12.87" width="0.254" layer="29"/>
<wire x1="55.44" y1="11.75" x2="57.74" y2="11.9" width="0.254" layer="29"/>
<wire x1="57.31" y1="11.03" x2="55.03" y2="10.52" width="0.254" layer="29"/>
<wire x1="56.19" y1="9.48" x2="58.19" y2="10.42" width="0.254" layer="29"/>
<wire x1="56.08" y1="8.22" x2="58.03" y2="9.37" width="0.254" layer="29"/>
<wire x1="59.15" y1="9.16" x2="57.28" y2="7.58" width="0.254" layer="29"/>
<wire x1="59.44" y1="8.13" x2="58.1" y2="6.28" width="0.254" layer="29"/>
<wire x1="59.46" y1="6.19" x2="60.45" y2="8.25" width="0.254" layer="29"/>
<wire x1="60.91" y1="7.55" x2="60.15" y2="5.37" width="0.254" layer="29"/>
<wire x1="61.49" y1="5.66" x2="61.84" y2="7.99" width="0.254" layer="29"/>
<wire x1="62.49" y1="7.29" x2="62.42" y2="5.06" width="0.254" layer="29"/>
<wire x1="63.64" y1="5.74" x2="63.18" y2="8.09" width="0.254" layer="29"/>
<wire x1="64.05" y1="7.62" x2="64.83" y2="5.57" width="0.254" layer="29"/>
<wire x1="65.83" y1="6.52" x2="64.58" y2="8.66" width="0.254" layer="29"/>
<wire x1="65.57" y1="8.44" x2="66.77" y2="6.66" width="0.254" layer="29"/>
<wire x1="65.66" y1="9.6" x2="67.21" y2="7.98" width="0.254" layer="29"/>
<wire x1="66.64" y1="9.72" x2="68.39" y2="8.42" width="0.254" layer="29"/>
<wire x1="66.43" y1="10.93" x2="68.56" y2="9.88" width="0.254" layer="29"/>
<wire x1="67.25" y1="11.38" x2="69.37" y2="10.7" width="0.254" layer="29"/>
<wire x1="66.71" y1="12.07" x2="69.07" y2="11.91" width="0.254" layer="29"/>
<wire x1="67.37" y1="12.77" x2="69.66" y2="12.74" width="0.254" layer="29"/>
<wire x1="66.67" y1="13.36" x2="69.01" y2="13.68" width="0.254" layer="29"/>
<wire x1="67.09" y1="14.32" x2="69.33" y2="14.74" width="0.254" layer="29"/>
<wire x1="66.23" y1="14.68" x2="68.4" y2="15.65" width="0.254" layer="29"/>
<wire x1="66.34" y1="15.55" x2="68.37" y2="16.64" width="0.254" layer="29"/>
<wire x1="65.39" y1="15.77" x2="67.21" y2="17.2" width="0.254" layer="29"/>
<wire x1="66.73" y1="18.42" x2="65.17" y2="16.64" width="0.254" layer="29"/>
<wire x1="64.2" y1="16.6" x2="65.54" y2="18.51" width="0.254" layer="29"/>
<wire x1="63.7" y1="17.52" x2="64.54" y2="19.54" width="0.254" layer="29"/>
<wire x1="62.76" y1="17.01" x2="63.3" y2="19.29" width="0.254" layer="29"/>
<wire x1="62.2" y1="17.87" x2="62.2" y2="19.99" width="0.254" layer="29"/>
<wire x1="61.62" y1="16.98" x2="61.21" y2="19.45" width="0.254" layer="29"/>
<wire x1="60.88" y1="17.68" x2="60.02" y2="19.65" width="0.254" layer="29"/>
<wire x1="59.35" y1="18.78" x2="60.46" y2="16.8" width="0.254" layer="29"/>
<wire x1="58.16" y1="18.74" x2="59.61" y2="16.86" width="0.254" layer="29"/>
<wire x1="57.76" y1="17.74" x2="59.32" y2="16.02" width="0.254" layer="29"/>
<wire x1="56.68" y1="17.51" x2="58.2" y2="16" width="0.254" layer="29"/>
<wire x1="56.43" y1="16.16" x2="58.29" y2="14.93" width="0.254" layer="29"/>
<wire x1="55.36" y1="15.53" x2="57.42" y2="14.5" width="0.254" layer="29"/>
<wire x1="81.7" y1="17.5" x2="83.2" y2="16" width="0.254" layer="1"/>
<wire x1="83.35" y1="14.88" x2="81.43" y2="16.16" width="0.254" layer="1"/>
<wire x1="80.36" y1="15.54" x2="82.42" y2="14.5" width="0.254" layer="1"/>
<wire x1="79.72" y1="13.01" x2="82.02" y2="12.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="11.89" x2="80.44" y2="11.75" width="0.254" layer="1"/>
<wire x1="80.04" y1="10.52" x2="82.31" y2="11.03" width="0.254" layer="1"/>
<wire x1="83.19" y1="10.42" x2="81.19" y2="9.48" width="0.254" layer="1"/>
<wire x1="81.13" y1="8.25" x2="83.03" y2="9.37" width="0.254" layer="1"/>
<wire x1="84.13" y1="9.13" x2="82.28" y2="7.58" width="0.254" layer="1"/>
<wire x1="83.11" y1="6.33" x2="84.44" y2="8.13" width="0.254" layer="1"/>
<wire x1="85.46" y1="8.28" x2="84.47" y2="6.19" width="0.254" layer="1"/>
<wire x1="85.17" y1="5.42" x2="85.91" y2="7.55" width="0.254" layer="1"/>
<wire x1="86.83" y1="7.93" x2="86.49" y2="5.66" width="0.254" layer="1"/>
<wire x1="87.42" y1="5.06" x2="87.49" y2="7.29" width="0.254" layer="1"/>
<wire x1="88.19" y1="8.04" x2="88.64" y2="5.74" width="0.254" layer="1"/>
<wire x1="89.79" y1="5.69" x2="89.05" y2="7.62" width="0.254" layer="1"/>
<wire x1="89.61" y1="8.6" x2="90.83" y2="6.51" width="0.254" layer="1"/>
<wire x1="91.74" y1="6.7" x2="90.57" y2="8.44" width="0.254" layer="1"/>
<wire x1="90.7" y1="9.56" x2="92.21" y2="7.98" width="0.254" layer="1"/>
<wire x1="93.37" y1="8.44" x2="91.64" y2="9.72" width="0.254" layer="1"/>
<wire x1="91.5" y1="10.9" x2="93.56" y2="9.88" width="0.254" layer="1"/>
<wire x1="94.42" y1="10.68" x2="92.25" y2="11.38" width="0.254" layer="1"/>
<wire x1="91.8" y1="12.07" x2="94.07" y2="11.91" width="0.254" layer="1"/>
<wire x1="94.58" y1="12.74" x2="92.38" y2="12.77" width="0.254" layer="1"/>
<wire x1="91.61" y1="13.36" x2="94.01" y2="13.68" width="0.254" layer="1"/>
<wire x1="94.31" y1="14.74" x2="92.09" y2="14.32" width="0.254" layer="1"/>
<wire x1="91.18" y1="14.66" x2="93.4" y2="15.65" width="0.254" layer="1"/>
<wire x1="93.35" y1="16.62" x2="91.34" y2="15.55" width="0.254" layer="1"/>
<wire x1="90.38" y1="15.76" x2="92.21" y2="17.2" width="0.254" layer="1"/>
<wire x1="91.72" y1="18.41" x2="90.17" y2="16.64" width="0.254" layer="1"/>
<wire x1="89.18" y1="16.59" x2="90.54" y2="18.51" width="0.254" layer="1"/>
<wire x1="89.53" y1="19.53" x2="88.7" y2="17.52" width="0.254" layer="1"/>
<wire x1="87.79" y1="17.12" x2="88.3" y2="19.29" width="0.254" layer="1"/>
<wire x1="84.29" y1="16.05" x2="82.76" y2="17.74" width="0.254" layer="1"/>
<wire x1="83.19" y1="18.7" x2="84.61" y2="16.86" width="0.254" layer="1"/>
<wire x1="85.46" y1="16.8" x2="84.35" y2="18.78" width="0.254" layer="1"/>
<wire x1="85.04" y1="19.61" x2="85.88" y2="17.68" width="0.254" layer="1"/>
<wire x1="86.59" y1="17.07" x2="86.21" y2="19.45" width="0.254" layer="1"/>
<wire x1="87.2" y1="19.96" x2="87.2" y2="17.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="13.5" x2="80.41" y2="14.24" width="0.254" layer="1"/>
<wire x1="80.41" y1="14.24" x2="82.78" y2="13.48" width="0.254" layer="29"/>
<wire x1="79.73" y1="13.01" x2="82.02" y2="12.87" width="0.254" layer="29"/>
<wire x1="80.44" y1="11.75" x2="82.74" y2="11.9" width="0.254" layer="29"/>
<wire x1="82.31" y1="11.03" x2="80.03" y2="10.52" width="0.254" layer="29"/>
<wire x1="81.19" y1="9.48" x2="83.19" y2="10.42" width="0.254" layer="29"/>
<wire x1="81.08" y1="8.22" x2="83.03" y2="9.37" width="0.254" layer="29"/>
<wire x1="84.15" y1="9.16" x2="82.28" y2="7.58" width="0.254" layer="29"/>
<wire x1="84.44" y1="8.13" x2="83.1" y2="6.28" width="0.254" layer="29"/>
<wire x1="84.46" y1="6.19" x2="85.45" y2="8.25" width="0.254" layer="29"/>
<wire x1="85.91" y1="7.55" x2="85.15" y2="5.37" width="0.254" layer="29"/>
<wire x1="86.49" y1="5.66" x2="86.84" y2="7.99" width="0.254" layer="29"/>
<wire x1="87.49" y1="7.29" x2="87.42" y2="5.06" width="0.254" layer="29"/>
<wire x1="88.64" y1="5.74" x2="88.18" y2="8.09" width="0.254" layer="29"/>
<wire x1="89.05" y1="7.62" x2="89.83" y2="5.57" width="0.254" layer="29"/>
<wire x1="90.83" y1="6.52" x2="89.58" y2="8.66" width="0.254" layer="29"/>
<wire x1="90.57" y1="8.44" x2="91.77" y2="6.66" width="0.254" layer="29"/>
<wire x1="90.66" y1="9.6" x2="92.21" y2="7.98" width="0.254" layer="29"/>
<wire x1="91.64" y1="9.72" x2="93.39" y2="8.42" width="0.254" layer="29"/>
<wire x1="91.43" y1="10.93" x2="93.56" y2="9.88" width="0.254" layer="29"/>
<wire x1="92.25" y1="11.38" x2="94.37" y2="10.7" width="0.254" layer="29"/>
<wire x1="91.71" y1="12.07" x2="94.07" y2="11.91" width="0.254" layer="29"/>
<wire x1="92.37" y1="12.77" x2="94.66" y2="12.74" width="0.254" layer="29"/>
<wire x1="91.67" y1="13.36" x2="94.01" y2="13.68" width="0.254" layer="29"/>
<wire x1="92.09" y1="14.32" x2="94.33" y2="14.74" width="0.254" layer="29"/>
<wire x1="91.23" y1="14.68" x2="93.4" y2="15.65" width="0.254" layer="29"/>
<wire x1="91.34" y1="15.55" x2="93.37" y2="16.64" width="0.254" layer="29"/>
<wire x1="90.39" y1="15.77" x2="92.21" y2="17.2" width="0.254" layer="29"/>
<wire x1="91.73" y1="18.42" x2="90.17" y2="16.64" width="0.254" layer="29"/>
<wire x1="89.2" y1="16.6" x2="90.54" y2="18.51" width="0.254" layer="29"/>
<wire x1="88.7" y1="17.52" x2="89.54" y2="19.54" width="0.254" layer="29"/>
<wire x1="87.76" y1="17.01" x2="88.3" y2="19.29" width="0.254" layer="29"/>
<wire x1="87.2" y1="17.87" x2="87.2" y2="19.99" width="0.254" layer="29"/>
<wire x1="86.62" y1="16.98" x2="86.21" y2="19.45" width="0.254" layer="29"/>
<wire x1="85.88" y1="17.68" x2="85.02" y2="19.65" width="0.254" layer="29"/>
<wire x1="84.35" y1="18.78" x2="85.46" y2="16.8" width="0.254" layer="29"/>
<wire x1="83.16" y1="18.74" x2="84.61" y2="16.86" width="0.254" layer="29"/>
<wire x1="82.76" y1="17.74" x2="84.32" y2="16.02" width="0.254" layer="29"/>
<wire x1="81.68" y1="17.51" x2="83.2" y2="16" width="0.254" layer="29"/>
<wire x1="81.43" y1="16.16" x2="83.29" y2="14.93" width="0.254" layer="29"/>
<wire x1="80.36" y1="15.53" x2="82.42" y2="14.5" width="0.254" layer="29"/>
<wire x1="0" y1="0" x2="0" y2="100" width="0.254" layer="21"/>
<wire x1="6.7" y1="92.5" x2="8.2" y2="91" width="0.254" layer="1"/>
<wire x1="8.35" y1="89.88" x2="6.43" y2="91.16" width="0.254" layer="1"/>
<wire x1="5.36" y1="90.54" x2="7.42" y2="89.5" width="0.254" layer="1"/>
<wire x1="4.72" y1="88.01" x2="7.02" y2="87.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="86.89" x2="5.44" y2="86.75" width="0.254" layer="1"/>
<wire x1="5.04" y1="85.52" x2="7.31" y2="86.03" width="0.254" layer="1"/>
<wire x1="8.19" y1="85.42" x2="6.19" y2="84.48" width="0.254" layer="1"/>
<wire x1="6.13" y1="83.25" x2="8.03" y2="84.37" width="0.254" layer="1"/>
<wire x1="9.13" y1="84.13" x2="7.28" y2="82.58" width="0.254" layer="1"/>
<wire x1="8.11" y1="81.33" x2="9.44" y2="83.13" width="0.254" layer="1"/>
<wire x1="10.46" y1="83.28" x2="9.47" y2="81.19" width="0.254" layer="1"/>
<wire x1="10.17" y1="80.42" x2="10.91" y2="82.55" width="0.254" layer="1"/>
<wire x1="11.83" y1="82.93" x2="11.49" y2="80.66" width="0.254" layer="1"/>
<wire x1="12.42" y1="80.06" x2="12.49" y2="82.29" width="0.254" layer="1"/>
<wire x1="13.19" y1="83.04" x2="13.64" y2="80.74" width="0.254" layer="1"/>
<wire x1="14.79" y1="80.69" x2="14.05" y2="82.62" width="0.254" layer="1"/>
<wire x1="14.61" y1="83.6" x2="15.83" y2="81.51" width="0.254" layer="1"/>
<wire x1="16.74" y1="81.7" x2="15.57" y2="83.44" width="0.254" layer="1"/>
<wire x1="15.7" y1="84.56" x2="17.21" y2="82.98" width="0.254" layer="1"/>
<wire x1="18.37" y1="83.44" x2="16.64" y2="84.72" width="0.254" layer="1"/>
<wire x1="16.5" y1="85.9" x2="18.56" y2="84.88" width="0.254" layer="1"/>
<wire x1="19.42" y1="85.68" x2="17.25" y2="86.38" width="0.254" layer="1"/>
<wire x1="16.8" y1="87.07" x2="19.07" y2="86.91" width="0.254" layer="1"/>
<wire x1="19.58" y1="87.74" x2="17.38" y2="87.77" width="0.254" layer="1"/>
<wire x1="16.61" y1="88.36" x2="19.01" y2="88.68" width="0.254" layer="1"/>
<wire x1="19.31" y1="89.74" x2="17.09" y2="89.32" width="0.254" layer="1"/>
<wire x1="16.18" y1="89.66" x2="18.4" y2="90.65" width="0.254" layer="1"/>
<wire x1="18.35" y1="91.62" x2="16.34" y2="90.55" width="0.254" layer="1"/>
<wire x1="15.38" y1="90.76" x2="17.21" y2="92.2" width="0.254" layer="1"/>
<wire x1="16.72" y1="93.41" x2="15.17" y2="91.64" width="0.254" layer="1"/>
<wire x1="14.18" y1="91.59" x2="15.54" y2="93.51" width="0.254" layer="1"/>
<wire x1="14.53" y1="94.53" x2="13.7" y2="92.52" width="0.254" layer="1"/>
<wire x1="12.79" y1="92.12" x2="13.3" y2="94.29" width="0.254" layer="1"/>
<wire x1="9.29" y1="91.05" x2="7.76" y2="92.74" width="0.254" layer="1"/>
<wire x1="8.19" y1="93.7" x2="9.61" y2="91.86" width="0.254" layer="1"/>
<wire x1="10.46" y1="91.8" x2="9.35" y2="93.78" width="0.254" layer="1"/>
<wire x1="10.04" y1="94.61" x2="10.88" y2="92.68" width="0.254" layer="1"/>
<wire x1="11.59" y1="92.07" x2="11.21" y2="94.45" width="0.254" layer="1"/>
<wire x1="12.2" y1="94.96" x2="12.2" y2="92.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="88.5" x2="5.41" y2="89.24" width="0.254" layer="1"/>
<wire x1="5.41" y1="89.24" x2="7.78" y2="88.48" width="0.254" layer="29"/>
<wire x1="4.73" y1="88.01" x2="7.02" y2="87.87" width="0.254" layer="29"/>
<wire x1="5.44" y1="86.75" x2="7.74" y2="86.9" width="0.254" layer="29"/>
<wire x1="7.31" y1="86.03" x2="5.03" y2="85.52" width="0.254" layer="29"/>
<wire x1="6.19" y1="84.48" x2="8.19" y2="85.42" width="0.254" layer="29"/>
<wire x1="6.08" y1="83.22" x2="8.03" y2="84.37" width="0.254" layer="29"/>
<wire x1="9.15" y1="84.16" x2="7.28" y2="82.58" width="0.254" layer="29"/>
<wire x1="9.44" y1="83.13" x2="8.1" y2="81.28" width="0.254" layer="29"/>
<wire x1="9.46" y1="81.19" x2="10.45" y2="83.25" width="0.254" layer="29"/>
<wire x1="10.91" y1="82.55" x2="10.15" y2="80.37" width="0.254" layer="29"/>
<wire x1="11.49" y1="80.66" x2="11.84" y2="82.99" width="0.254" layer="29"/>
<wire x1="12.49" y1="82.29" x2="12.42" y2="80.06" width="0.254" layer="29"/>
<wire x1="13.64" y1="80.74" x2="13.18" y2="83.09" width="0.254" layer="29"/>
<wire x1="14.05" y1="82.62" x2="14.83" y2="80.57" width="0.254" layer="29"/>
<wire x1="15.83" y1="81.52" x2="14.58" y2="83.66" width="0.254" layer="29"/>
<wire x1="15.57" y1="83.44" x2="16.77" y2="81.66" width="0.254" layer="29"/>
<wire x1="15.66" y1="84.6" x2="17.21" y2="82.98" width="0.254" layer="29"/>
<wire x1="16.64" y1="84.72" x2="18.39" y2="83.42" width="0.254" layer="29"/>
<wire x1="16.43" y1="85.93" x2="18.56" y2="84.88" width="0.254" layer="29"/>
<wire x1="17.25" y1="86.38" x2="19.37" y2="85.7" width="0.254" layer="29"/>
<wire x1="16.71" y1="87.07" x2="19.07" y2="86.91" width="0.254" layer="29"/>
<wire x1="17.37" y1="87.77" x2="19.66" y2="87.74" width="0.254" layer="29"/>
<wire x1="16.67" y1="88.36" x2="19.01" y2="88.68" width="0.254" layer="29"/>
<wire x1="17.09" y1="89.32" x2="19.33" y2="89.74" width="0.254" layer="29"/>
<wire x1="16.23" y1="89.68" x2="18.4" y2="90.65" width="0.254" layer="29"/>
<wire x1="16.34" y1="90.55" x2="18.37" y2="91.64" width="0.254" layer="29"/>
<wire x1="15.39" y1="90.77" x2="17.21" y2="92.2" width="0.254" layer="29"/>
<wire x1="16.73" y1="93.42" x2="15.17" y2="91.64" width="0.254" layer="29"/>
<wire x1="14.2" y1="91.6" x2="15.54" y2="93.51" width="0.254" layer="29"/>
<wire x1="13.7" y1="92.52" x2="14.54" y2="94.54" width="0.254" layer="29"/>
<wire x1="12.76" y1="92.01" x2="13.3" y2="94.29" width="0.254" layer="29"/>
<wire x1="12.2" y1="92.87" x2="12.2" y2="94.99" width="0.254" layer="29"/>
<wire x1="11.62" y1="91.98" x2="11.21" y2="94.45" width="0.254" layer="29"/>
<wire x1="10.88" y1="92.68" x2="10.02" y2="94.65" width="0.254" layer="29"/>
<wire x1="9.35" y1="93.78" x2="10.46" y2="91.8" width="0.254" layer="29"/>
<wire x1="8.16" y1="93.74" x2="9.61" y2="91.86" width="0.254" layer="29"/>
<wire x1="7.76" y1="92.74" x2="9.32" y2="91.02" width="0.254" layer="29"/>
<wire x1="6.68" y1="92.51" x2="8.2" y2="91" width="0.254" layer="29"/>
<wire x1="6.43" y1="91.16" x2="8.29" y2="89.93" width="0.254" layer="29"/>
<wire x1="5.36" y1="90.53" x2="7.42" y2="89.5" width="0.254" layer="29"/>
<wire x1="31.7" y1="92.5" x2="33.2" y2="91" width="0.254" layer="1"/>
<wire x1="33.35" y1="89.88" x2="31.43" y2="91.16" width="0.254" layer="1"/>
<wire x1="30.36" y1="90.54" x2="32.42" y2="89.5" width="0.254" layer="1"/>
<wire x1="29.72" y1="88.01" x2="32.02" y2="87.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="86.89" x2="30.44" y2="86.75" width="0.254" layer="1"/>
<wire x1="30.04" y1="85.52" x2="32.31" y2="86.03" width="0.254" layer="1"/>
<wire x1="33.19" y1="85.42" x2="31.19" y2="84.48" width="0.254" layer="1"/>
<wire x1="31.13" y1="83.25" x2="33.03" y2="84.37" width="0.254" layer="1"/>
<wire x1="34.13" y1="84.13" x2="32.28" y2="82.58" width="0.254" layer="1"/>
<wire x1="33.11" y1="81.33" x2="34.44" y2="83.13" width="0.254" layer="1"/>
<wire x1="35.46" y1="83.28" x2="34.47" y2="81.19" width="0.254" layer="1"/>
<wire x1="35.17" y1="80.42" x2="35.91" y2="82.55" width="0.254" layer="1"/>
<wire x1="36.83" y1="82.93" x2="36.49" y2="80.66" width="0.254" layer="1"/>
<wire x1="37.42" y1="80.06" x2="37.49" y2="82.29" width="0.254" layer="1"/>
<wire x1="38.19" y1="83.04" x2="38.64" y2="80.74" width="0.254" layer="1"/>
<wire x1="39.79" y1="80.69" x2="39.05" y2="82.62" width="0.254" layer="1"/>
<wire x1="39.61" y1="83.6" x2="40.83" y2="81.51" width="0.254" layer="1"/>
<wire x1="41.74" y1="81.7" x2="40.57" y2="83.44" width="0.254" layer="1"/>
<wire x1="40.7" y1="84.56" x2="42.21" y2="82.98" width="0.254" layer="1"/>
<wire x1="43.37" y1="83.44" x2="41.64" y2="84.72" width="0.254" layer="1"/>
<wire x1="41.5" y1="85.9" x2="43.56" y2="84.88" width="0.254" layer="1"/>
<wire x1="44.42" y1="85.68" x2="42.25" y2="86.38" width="0.254" layer="1"/>
<wire x1="41.8" y1="87.07" x2="44.07" y2="86.91" width="0.254" layer="1"/>
<wire x1="44.58" y1="87.74" x2="42.38" y2="87.77" width="0.254" layer="1"/>
<wire x1="41.61" y1="88.36" x2="44.01" y2="88.68" width="0.254" layer="1"/>
<wire x1="44.31" y1="89.74" x2="42.09" y2="89.32" width="0.254" layer="1"/>
<wire x1="41.18" y1="89.66" x2="43.4" y2="90.65" width="0.254" layer="1"/>
<wire x1="43.35" y1="91.62" x2="41.34" y2="90.55" width="0.254" layer="1"/>
<wire x1="40.38" y1="90.76" x2="42.21" y2="92.2" width="0.254" layer="1"/>
<wire x1="41.72" y1="93.41" x2="40.17" y2="91.64" width="0.254" layer="1"/>
<wire x1="39.18" y1="91.59" x2="40.54" y2="93.51" width="0.254" layer="1"/>
<wire x1="39.53" y1="94.53" x2="38.7" y2="92.52" width="0.254" layer="1"/>
<wire x1="37.79" y1="92.12" x2="38.3" y2="94.29" width="0.254" layer="1"/>
<wire x1="34.29" y1="91.05" x2="32.76" y2="92.74" width="0.254" layer="1"/>
<wire x1="33.19" y1="93.7" x2="34.61" y2="91.86" width="0.254" layer="1"/>
<wire x1="35.46" y1="91.8" x2="34.35" y2="93.78" width="0.254" layer="1"/>
<wire x1="35.04" y1="94.61" x2="35.88" y2="92.68" width="0.254" layer="1"/>
<wire x1="36.59" y1="92.07" x2="36.21" y2="94.45" width="0.254" layer="1"/>
<wire x1="37.2" y1="94.96" x2="37.2" y2="92.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="88.5" x2="30.41" y2="89.24" width="0.254" layer="1"/>
<wire x1="30.41" y1="89.24" x2="32.78" y2="88.48" width="0.254" layer="29"/>
<wire x1="29.73" y1="88.01" x2="32.02" y2="87.87" width="0.254" layer="29"/>
<wire x1="30.44" y1="86.75" x2="32.74" y2="86.9" width="0.254" layer="29"/>
<wire x1="32.31" y1="86.03" x2="30.03" y2="85.52" width="0.254" layer="29"/>
<wire x1="31.19" y1="84.48" x2="33.19" y2="85.42" width="0.254" layer="29"/>
<wire x1="31.08" y1="83.22" x2="33.03" y2="84.37" width="0.254" layer="29"/>
<wire x1="34.15" y1="84.16" x2="32.28" y2="82.58" width="0.254" layer="29"/>
<wire x1="34.44" y1="83.13" x2="33.1" y2="81.28" width="0.254" layer="29"/>
<wire x1="34.46" y1="81.19" x2="35.45" y2="83.25" width="0.254" layer="29"/>
<wire x1="35.91" y1="82.55" x2="35.15" y2="80.37" width="0.254" layer="29"/>
<wire x1="36.49" y1="80.66" x2="36.84" y2="82.99" width="0.254" layer="29"/>
<wire x1="37.49" y1="82.29" x2="37.42" y2="80.06" width="0.254" layer="29"/>
<wire x1="38.64" y1="80.74" x2="38.18" y2="83.09" width="0.254" layer="29"/>
<wire x1="39.05" y1="82.62" x2="39.83" y2="80.57" width="0.254" layer="29"/>
<wire x1="40.83" y1="81.52" x2="39.58" y2="83.66" width="0.254" layer="29"/>
<wire x1="40.57" y1="83.44" x2="41.77" y2="81.66" width="0.254" layer="29"/>
<wire x1="40.66" y1="84.6" x2="42.21" y2="82.98" width="0.254" layer="29"/>
<wire x1="41.64" y1="84.72" x2="43.39" y2="83.42" width="0.254" layer="29"/>
<wire x1="41.43" y1="85.93" x2="43.56" y2="84.88" width="0.254" layer="29"/>
<wire x1="42.25" y1="86.38" x2="44.37" y2="85.7" width="0.254" layer="29"/>
<wire x1="41.71" y1="87.07" x2="44.07" y2="86.91" width="0.254" layer="29"/>
<wire x1="42.37" y1="87.77" x2="44.66" y2="87.74" width="0.254" layer="29"/>
<wire x1="41.67" y1="88.36" x2="44.01" y2="88.68" width="0.254" layer="29"/>
<wire x1="42.09" y1="89.32" x2="44.33" y2="89.74" width="0.254" layer="29"/>
<wire x1="41.23" y1="89.68" x2="43.4" y2="90.65" width="0.254" layer="29"/>
<wire x1="41.34" y1="90.55" x2="43.37" y2="91.64" width="0.254" layer="29"/>
<wire x1="40.39" y1="90.77" x2="42.21" y2="92.2" width="0.254" layer="29"/>
<wire x1="41.73" y1="93.42" x2="40.17" y2="91.64" width="0.254" layer="29"/>
<wire x1="39.2" y1="91.6" x2="40.54" y2="93.51" width="0.254" layer="29"/>
<wire x1="38.7" y1="92.52" x2="39.54" y2="94.54" width="0.254" layer="29"/>
<wire x1="37.76" y1="92.01" x2="38.3" y2="94.29" width="0.254" layer="29"/>
<wire x1="37.2" y1="92.87" x2="37.2" y2="94.99" width="0.254" layer="29"/>
<wire x1="36.62" y1="91.98" x2="36.21" y2="94.45" width="0.254" layer="29"/>
<wire x1="35.88" y1="92.68" x2="35.02" y2="94.65" width="0.254" layer="29"/>
<wire x1="34.35" y1="93.78" x2="35.46" y2="91.8" width="0.254" layer="29"/>
<wire x1="33.16" y1="93.74" x2="34.61" y2="91.86" width="0.254" layer="29"/>
<wire x1="32.76" y1="92.74" x2="34.32" y2="91.02" width="0.254" layer="29"/>
<wire x1="31.68" y1="92.51" x2="33.2" y2="91" width="0.254" layer="29"/>
<wire x1="31.43" y1="91.16" x2="33.29" y2="89.93" width="0.254" layer="29"/>
<wire x1="30.36" y1="90.53" x2="32.42" y2="89.5" width="0.254" layer="29"/>
<wire x1="6.7" y1="67.5" x2="8.2" y2="66" width="0.254" layer="1"/>
<wire x1="8.35" y1="64.88" x2="6.43" y2="66.16" width="0.254" layer="1"/>
<wire x1="5.36" y1="65.54" x2="7.42" y2="64.5" width="0.254" layer="1"/>
<wire x1="4.72" y1="63.01" x2="7.02" y2="62.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="61.89" x2="5.44" y2="61.75" width="0.254" layer="1"/>
<wire x1="5.04" y1="60.52" x2="7.31" y2="61.03" width="0.254" layer="1"/>
<wire x1="8.19" y1="60.42" x2="6.19" y2="59.48" width="0.254" layer="1"/>
<wire x1="6.13" y1="58.25" x2="8.03" y2="59.37" width="0.254" layer="1"/>
<wire x1="9.13" y1="59.13" x2="7.28" y2="57.58" width="0.254" layer="1"/>
<wire x1="8.11" y1="56.33" x2="9.44" y2="58.13" width="0.254" layer="1"/>
<wire x1="10.46" y1="58.28" x2="9.47" y2="56.19" width="0.254" layer="1"/>
<wire x1="10.17" y1="55.42" x2="10.91" y2="57.55" width="0.254" layer="1"/>
<wire x1="11.83" y1="57.93" x2="11.49" y2="55.66" width="0.254" layer="1"/>
<wire x1="12.42" y1="55.06" x2="12.49" y2="57.29" width="0.254" layer="1"/>
<wire x1="13.19" y1="58.04" x2="13.64" y2="55.74" width="0.254" layer="1"/>
<wire x1="14.79" y1="55.69" x2="14.05" y2="57.62" width="0.254" layer="1"/>
<wire x1="14.61" y1="58.6" x2="15.83" y2="56.51" width="0.254" layer="1"/>
<wire x1="16.74" y1="56.7" x2="15.57" y2="58.44" width="0.254" layer="1"/>
<wire x1="15.7" y1="59.56" x2="17.21" y2="57.98" width="0.254" layer="1"/>
<wire x1="18.37" y1="58.44" x2="16.64" y2="59.72" width="0.254" layer="1"/>
<wire x1="16.5" y1="60.9" x2="18.56" y2="59.88" width="0.254" layer="1"/>
<wire x1="19.42" y1="60.68" x2="17.25" y2="61.38" width="0.254" layer="1"/>
<wire x1="16.8" y1="62.07" x2="19.07" y2="61.91" width="0.254" layer="1"/>
<wire x1="19.58" y1="62.74" x2="17.38" y2="62.77" width="0.254" layer="1"/>
<wire x1="16.61" y1="63.36" x2="19.01" y2="63.68" width="0.254" layer="1"/>
<wire x1="19.31" y1="64.74" x2="17.09" y2="64.32" width="0.254" layer="1"/>
<wire x1="16.18" y1="64.66" x2="18.4" y2="65.65" width="0.254" layer="1"/>
<wire x1="18.35" y1="66.62" x2="16.34" y2="65.55" width="0.254" layer="1"/>
<wire x1="15.38" y1="65.76" x2="17.21" y2="67.2" width="0.254" layer="1"/>
<wire x1="16.72" y1="68.41" x2="15.17" y2="66.64" width="0.254" layer="1"/>
<wire x1="14.18" y1="66.59" x2="15.54" y2="68.51" width="0.254" layer="1"/>
<wire x1="14.53" y1="69.53" x2="13.7" y2="67.52" width="0.254" layer="1"/>
<wire x1="12.79" y1="67.12" x2="13.3" y2="69.29" width="0.254" layer="1"/>
<wire x1="9.29" y1="66.05" x2="7.76" y2="67.74" width="0.254" layer="1"/>
<wire x1="8.19" y1="68.7" x2="9.61" y2="66.86" width="0.254" layer="1"/>
<wire x1="10.46" y1="66.8" x2="9.35" y2="68.78" width="0.254" layer="1"/>
<wire x1="10.04" y1="69.61" x2="10.88" y2="67.68" width="0.254" layer="1"/>
<wire x1="11.59" y1="67.07" x2="11.21" y2="69.45" width="0.254" layer="1"/>
<wire x1="12.2" y1="69.96" x2="12.2" y2="67.87" width="0.254" layer="1"/>
<wire x1="7.74" y1="63.5" x2="5.41" y2="64.24" width="0.254" layer="1"/>
<wire x1="5.41" y1="64.24" x2="7.78" y2="63.48" width="0.254" layer="29"/>
<wire x1="4.73" y1="63.01" x2="7.02" y2="62.87" width="0.254" layer="29"/>
<wire x1="5.44" y1="61.75" x2="7.74" y2="61.9" width="0.254" layer="29"/>
<wire x1="7.31" y1="61.03" x2="5.03" y2="60.52" width="0.254" layer="29"/>
<wire x1="6.19" y1="59.48" x2="8.19" y2="60.42" width="0.254" layer="29"/>
<wire x1="6.08" y1="58.22" x2="8.03" y2="59.37" width="0.254" layer="29"/>
<wire x1="9.15" y1="59.16" x2="7.28" y2="57.58" width="0.254" layer="29"/>
<wire x1="9.44" y1="58.13" x2="8.1" y2="56.28" width="0.254" layer="29"/>
<wire x1="9.46" y1="56.19" x2="10.45" y2="58.25" width="0.254" layer="29"/>
<wire x1="10.91" y1="57.55" x2="10.15" y2="55.37" width="0.254" layer="29"/>
<wire x1="11.49" y1="55.66" x2="11.84" y2="57.99" width="0.254" layer="29"/>
<wire x1="12.49" y1="57.29" x2="12.42" y2="55.06" width="0.254" layer="29"/>
<wire x1="13.64" y1="55.74" x2="13.18" y2="58.09" width="0.254" layer="29"/>
<wire x1="14.05" y1="57.62" x2="14.83" y2="55.57" width="0.254" layer="29"/>
<wire x1="15.83" y1="56.52" x2="14.58" y2="58.66" width="0.254" layer="29"/>
<wire x1="15.57" y1="58.44" x2="16.77" y2="56.66" width="0.254" layer="29"/>
<wire x1="15.66" y1="59.6" x2="17.21" y2="57.98" width="0.254" layer="29"/>
<wire x1="16.64" y1="59.72" x2="18.39" y2="58.42" width="0.254" layer="29"/>
<wire x1="16.43" y1="60.93" x2="18.56" y2="59.88" width="0.254" layer="29"/>
<wire x1="17.25" y1="61.38" x2="19.37" y2="60.7" width="0.254" layer="29"/>
<wire x1="16.71" y1="62.07" x2="19.07" y2="61.91" width="0.254" layer="29"/>
<wire x1="17.37" y1="62.77" x2="19.66" y2="62.74" width="0.254" layer="29"/>
<wire x1="16.67" y1="63.36" x2="19.01" y2="63.68" width="0.254" layer="29"/>
<wire x1="17.09" y1="64.32" x2="19.33" y2="64.74" width="0.254" layer="29"/>
<wire x1="16.23" y1="64.68" x2="18.4" y2="65.65" width="0.254" layer="29"/>
<wire x1="16.34" y1="65.55" x2="18.37" y2="66.64" width="0.254" layer="29"/>
<wire x1="15.39" y1="65.77" x2="17.21" y2="67.2" width="0.254" layer="29"/>
<wire x1="16.73" y1="68.42" x2="15.17" y2="66.64" width="0.254" layer="29"/>
<wire x1="14.2" y1="66.6" x2="15.54" y2="68.51" width="0.254" layer="29"/>
<wire x1="13.7" y1="67.52" x2="14.54" y2="69.54" width="0.254" layer="29"/>
<wire x1="12.76" y1="67.01" x2="13.3" y2="69.29" width="0.254" layer="29"/>
<wire x1="12.2" y1="67.87" x2="12.2" y2="69.99" width="0.254" layer="29"/>
<wire x1="11.62" y1="66.98" x2="11.21" y2="69.45" width="0.254" layer="29"/>
<wire x1="10.88" y1="67.68" x2="10.02" y2="69.65" width="0.254" layer="29"/>
<wire x1="9.35" y1="68.78" x2="10.46" y2="66.8" width="0.254" layer="29"/>
<wire x1="8.16" y1="68.74" x2="9.61" y2="66.86" width="0.254" layer="29"/>
<wire x1="7.76" y1="67.74" x2="9.32" y2="66.02" width="0.254" layer="29"/>
<wire x1="6.68" y1="67.51" x2="8.2" y2="66" width="0.254" layer="29"/>
<wire x1="6.43" y1="66.16" x2="8.29" y2="64.93" width="0.254" layer="29"/>
<wire x1="5.36" y1="65.53" x2="7.42" y2="64.5" width="0.254" layer="29"/>
<wire x1="31.7" y1="67.5" x2="33.2" y2="66" width="0.254" layer="1"/>
<wire x1="33.35" y1="64.88" x2="31.43" y2="66.16" width="0.254" layer="1"/>
<wire x1="30.36" y1="65.54" x2="32.42" y2="64.5" width="0.254" layer="1"/>
<wire x1="29.72" y1="63.01" x2="32.02" y2="62.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="61.89" x2="30.44" y2="61.75" width="0.254" layer="1"/>
<wire x1="30.04" y1="60.52" x2="32.31" y2="61.03" width="0.254" layer="1"/>
<wire x1="33.19" y1="60.42" x2="31.19" y2="59.48" width="0.254" layer="1"/>
<wire x1="31.13" y1="58.25" x2="33.03" y2="59.37" width="0.254" layer="1"/>
<wire x1="34.13" y1="59.13" x2="32.28" y2="57.58" width="0.254" layer="1"/>
<wire x1="33.11" y1="56.33" x2="34.44" y2="58.13" width="0.254" layer="1"/>
<wire x1="35.46" y1="58.28" x2="34.47" y2="56.19" width="0.254" layer="1"/>
<wire x1="35.17" y1="55.42" x2="35.91" y2="57.55" width="0.254" layer="1"/>
<wire x1="36.83" y1="57.93" x2="36.49" y2="55.66" width="0.254" layer="1"/>
<wire x1="37.42" y1="55.06" x2="37.49" y2="57.29" width="0.254" layer="1"/>
<wire x1="38.19" y1="58.04" x2="38.64" y2="55.74" width="0.254" layer="1"/>
<wire x1="39.79" y1="55.69" x2="39.05" y2="57.62" width="0.254" layer="1"/>
<wire x1="39.61" y1="58.6" x2="40.83" y2="56.51" width="0.254" layer="1"/>
<wire x1="41.74" y1="56.7" x2="40.57" y2="58.44" width="0.254" layer="1"/>
<wire x1="40.7" y1="59.56" x2="42.21" y2="57.98" width="0.254" layer="1"/>
<wire x1="43.37" y1="58.44" x2="41.64" y2="59.72" width="0.254" layer="1"/>
<wire x1="41.5" y1="60.9" x2="43.56" y2="59.88" width="0.254" layer="1"/>
<wire x1="44.42" y1="60.68" x2="42.25" y2="61.38" width="0.254" layer="1"/>
<wire x1="41.8" y1="62.07" x2="44.07" y2="61.91" width="0.254" layer="1"/>
<wire x1="44.58" y1="62.74" x2="42.38" y2="62.77" width="0.254" layer="1"/>
<wire x1="41.61" y1="63.36" x2="44.01" y2="63.68" width="0.254" layer="1"/>
<wire x1="44.31" y1="64.74" x2="42.09" y2="64.32" width="0.254" layer="1"/>
<wire x1="41.18" y1="64.66" x2="43.4" y2="65.65" width="0.254" layer="1"/>
<wire x1="43.35" y1="66.62" x2="41.34" y2="65.55" width="0.254" layer="1"/>
<wire x1="40.38" y1="65.76" x2="42.21" y2="67.2" width="0.254" layer="1"/>
<wire x1="41.72" y1="68.41" x2="40.17" y2="66.64" width="0.254" layer="1"/>
<wire x1="39.18" y1="66.59" x2="40.54" y2="68.51" width="0.254" layer="1"/>
<wire x1="39.53" y1="69.53" x2="38.7" y2="67.52" width="0.254" layer="1"/>
<wire x1="37.79" y1="67.12" x2="38.3" y2="69.29" width="0.254" layer="1"/>
<wire x1="34.29" y1="66.05" x2="32.76" y2="67.74" width="0.254" layer="1"/>
<wire x1="33.19" y1="68.7" x2="34.61" y2="66.86" width="0.254" layer="1"/>
<wire x1="35.46" y1="66.8" x2="34.35" y2="68.78" width="0.254" layer="1"/>
<wire x1="35.04" y1="69.61" x2="35.88" y2="67.68" width="0.254" layer="1"/>
<wire x1="36.59" y1="67.07" x2="36.21" y2="69.45" width="0.254" layer="1"/>
<wire x1="37.2" y1="69.96" x2="37.2" y2="67.87" width="0.254" layer="1"/>
<wire x1="32.74" y1="63.5" x2="30.41" y2="64.24" width="0.254" layer="1"/>
<wire x1="30.41" y1="64.24" x2="32.78" y2="63.48" width="0.254" layer="29"/>
<wire x1="29.73" y1="63.01" x2="32.02" y2="62.87" width="0.254" layer="29"/>
<wire x1="30.44" y1="61.75" x2="32.74" y2="61.9" width="0.254" layer="29"/>
<wire x1="32.31" y1="61.03" x2="30.03" y2="60.52" width="0.254" layer="29"/>
<wire x1="31.19" y1="59.48" x2="33.19" y2="60.42" width="0.254" layer="29"/>
<wire x1="31.08" y1="58.22" x2="33.03" y2="59.37" width="0.254" layer="29"/>
<wire x1="34.15" y1="59.16" x2="32.28" y2="57.58" width="0.254" layer="29"/>
<wire x1="34.44" y1="58.13" x2="33.1" y2="56.28" width="0.254" layer="29"/>
<wire x1="34.46" y1="56.19" x2="35.45" y2="58.25" width="0.254" layer="29"/>
<wire x1="35.91" y1="57.55" x2="35.15" y2="55.37" width="0.254" layer="29"/>
<wire x1="36.49" y1="55.66" x2="36.84" y2="57.99" width="0.254" layer="29"/>
<wire x1="37.49" y1="57.29" x2="37.42" y2="55.06" width="0.254" layer="29"/>
<wire x1="38.64" y1="55.74" x2="38.18" y2="58.09" width="0.254" layer="29"/>
<wire x1="39.05" y1="57.62" x2="39.83" y2="55.57" width="0.254" layer="29"/>
<wire x1="40.83" y1="56.52" x2="39.58" y2="58.66" width="0.254" layer="29"/>
<wire x1="40.57" y1="58.44" x2="41.77" y2="56.66" width="0.254" layer="29"/>
<wire x1="40.66" y1="59.6" x2="42.21" y2="57.98" width="0.254" layer="29"/>
<wire x1="41.64" y1="59.72" x2="43.39" y2="58.42" width="0.254" layer="29"/>
<wire x1="41.43" y1="60.93" x2="43.56" y2="59.88" width="0.254" layer="29"/>
<wire x1="42.25" y1="61.38" x2="44.37" y2="60.7" width="0.254" layer="29"/>
<wire x1="41.71" y1="62.07" x2="44.07" y2="61.91" width="0.254" layer="29"/>
<wire x1="42.37" y1="62.77" x2="44.66" y2="62.74" width="0.254" layer="29"/>
<wire x1="41.67" y1="63.36" x2="44.01" y2="63.68" width="0.254" layer="29"/>
<wire x1="42.09" y1="64.32" x2="44.33" y2="64.74" width="0.254" layer="29"/>
<wire x1="41.23" y1="64.68" x2="43.4" y2="65.65" width="0.254" layer="29"/>
<wire x1="41.34" y1="65.55" x2="43.37" y2="66.64" width="0.254" layer="29"/>
<wire x1="40.39" y1="65.77" x2="42.21" y2="67.2" width="0.254" layer="29"/>
<wire x1="41.73" y1="68.42" x2="40.17" y2="66.64" width="0.254" layer="29"/>
<wire x1="39.2" y1="66.6" x2="40.54" y2="68.51" width="0.254" layer="29"/>
<wire x1="38.7" y1="67.52" x2="39.54" y2="69.54" width="0.254" layer="29"/>
<wire x1="37.76" y1="67.01" x2="38.3" y2="69.29" width="0.254" layer="29"/>
<wire x1="37.2" y1="67.87" x2="37.2" y2="69.99" width="0.254" layer="29"/>
<wire x1="36.62" y1="66.98" x2="36.21" y2="69.45" width="0.254" layer="29"/>
<wire x1="35.88" y1="67.68" x2="35.02" y2="69.65" width="0.254" layer="29"/>
<wire x1="34.35" y1="68.78" x2="35.46" y2="66.8" width="0.254" layer="29"/>
<wire x1="33.16" y1="68.74" x2="34.61" y2="66.86" width="0.254" layer="29"/>
<wire x1="32.76" y1="67.74" x2="34.32" y2="66.02" width="0.254" layer="29"/>
<wire x1="31.68" y1="67.51" x2="33.2" y2="66" width="0.254" layer="29"/>
<wire x1="31.43" y1="66.16" x2="33.29" y2="64.93" width="0.254" layer="29"/>
<wire x1="30.36" y1="65.53" x2="32.42" y2="64.5" width="0.254" layer="29"/>
<wire x1="0" y1="100" x2="100" y2="100" width="0.254" layer="21"/>
<wire x1="100" y1="100" x2="100" y2="0" width="0.254" layer="21"/>
<wire x1="56.7" y1="92.5" x2="58.2" y2="91" width="0.254" layer="1"/>
<wire x1="58.35" y1="89.88" x2="56.43" y2="91.16" width="0.254" layer="1"/>
<wire x1="55.36" y1="90.54" x2="57.42" y2="89.5" width="0.254" layer="1"/>
<wire x1="54.72" y1="88.01" x2="57.02" y2="87.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="86.89" x2="55.44" y2="86.75" width="0.254" layer="1"/>
<wire x1="55.04" y1="85.52" x2="57.31" y2="86.03" width="0.254" layer="1"/>
<wire x1="58.19" y1="85.42" x2="56.19" y2="84.48" width="0.254" layer="1"/>
<wire x1="56.13" y1="83.25" x2="58.03" y2="84.37" width="0.254" layer="1"/>
<wire x1="59.13" y1="84.13" x2="57.28" y2="82.58" width="0.254" layer="1"/>
<wire x1="58.11" y1="81.33" x2="59.44" y2="83.13" width="0.254" layer="1"/>
<wire x1="60.46" y1="83.28" x2="59.47" y2="81.19" width="0.254" layer="1"/>
<wire x1="60.17" y1="80.42" x2="60.91" y2="82.55" width="0.254" layer="1"/>
<wire x1="61.83" y1="82.93" x2="61.49" y2="80.66" width="0.254" layer="1"/>
<wire x1="62.42" y1="80.06" x2="62.49" y2="82.29" width="0.254" layer="1"/>
<wire x1="63.19" y1="83.04" x2="63.64" y2="80.74" width="0.254" layer="1"/>
<wire x1="64.79" y1="80.69" x2="64.05" y2="82.62" width="0.254" layer="1"/>
<wire x1="64.61" y1="83.6" x2="65.83" y2="81.51" width="0.254" layer="1"/>
<wire x1="66.74" y1="81.7" x2="65.57" y2="83.44" width="0.254" layer="1"/>
<wire x1="65.7" y1="84.56" x2="67.21" y2="82.98" width="0.254" layer="1"/>
<wire x1="68.37" y1="83.44" x2="66.64" y2="84.72" width="0.254" layer="1"/>
<wire x1="66.5" y1="85.9" x2="68.56" y2="84.88" width="0.254" layer="1"/>
<wire x1="69.42" y1="85.68" x2="67.25" y2="86.38" width="0.254" layer="1"/>
<wire x1="66.8" y1="87.07" x2="69.07" y2="86.91" width="0.254" layer="1"/>
<wire x1="69.58" y1="87.74" x2="67.38" y2="87.77" width="0.254" layer="1"/>
<wire x1="66.61" y1="88.36" x2="69.01" y2="88.68" width="0.254" layer="1"/>
<wire x1="69.31" y1="89.74" x2="67.09" y2="89.32" width="0.254" layer="1"/>
<wire x1="66.18" y1="89.66" x2="68.4" y2="90.65" width="0.254" layer="1"/>
<wire x1="68.35" y1="91.62" x2="66.34" y2="90.55" width="0.254" layer="1"/>
<wire x1="65.38" y1="90.76" x2="67.21" y2="92.2" width="0.254" layer="1"/>
<wire x1="66.72" y1="93.41" x2="65.17" y2="91.64" width="0.254" layer="1"/>
<wire x1="64.18" y1="91.59" x2="65.54" y2="93.51" width="0.254" layer="1"/>
<wire x1="64.53" y1="94.53" x2="63.7" y2="92.52" width="0.254" layer="1"/>
<wire x1="62.79" y1="92.12" x2="63.3" y2="94.29" width="0.254" layer="1"/>
<wire x1="59.29" y1="91.05" x2="57.76" y2="92.74" width="0.254" layer="1"/>
<wire x1="58.19" y1="93.7" x2="59.61" y2="91.86" width="0.254" layer="1"/>
<wire x1="60.46" y1="91.8" x2="59.35" y2="93.78" width="0.254" layer="1"/>
<wire x1="60.04" y1="94.61" x2="60.88" y2="92.68" width="0.254" layer="1"/>
<wire x1="61.59" y1="92.07" x2="61.21" y2="94.45" width="0.254" layer="1"/>
<wire x1="62.2" y1="94.96" x2="62.2" y2="92.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="88.5" x2="55.41" y2="89.24" width="0.254" layer="1"/>
<wire x1="55.41" y1="89.24" x2="57.78" y2="88.48" width="0.254" layer="29"/>
<wire x1="54.73" y1="88.01" x2="57.02" y2="87.87" width="0.254" layer="29"/>
<wire x1="55.44" y1="86.75" x2="57.74" y2="86.9" width="0.254" layer="29"/>
<wire x1="57.31" y1="86.03" x2="55.03" y2="85.52" width="0.254" layer="29"/>
<wire x1="56.19" y1="84.48" x2="58.19" y2="85.42" width="0.254" layer="29"/>
<wire x1="56.08" y1="83.22" x2="58.03" y2="84.37" width="0.254" layer="29"/>
<wire x1="59.15" y1="84.16" x2="57.28" y2="82.58" width="0.254" layer="29"/>
<wire x1="59.44" y1="83.13" x2="58.1" y2="81.28" width="0.254" layer="29"/>
<wire x1="59.46" y1="81.19" x2="60.45" y2="83.25" width="0.254" layer="29"/>
<wire x1="60.91" y1="82.55" x2="60.15" y2="80.37" width="0.254" layer="29"/>
<wire x1="61.49" y1="80.66" x2="61.84" y2="82.99" width="0.254" layer="29"/>
<wire x1="62.49" y1="82.29" x2="62.42" y2="80.06" width="0.254" layer="29"/>
<wire x1="63.64" y1="80.74" x2="63.18" y2="83.09" width="0.254" layer="29"/>
<wire x1="64.05" y1="82.62" x2="64.83" y2="80.57" width="0.254" layer="29"/>
<wire x1="65.83" y1="81.52" x2="64.58" y2="83.66" width="0.254" layer="29"/>
<wire x1="65.57" y1="83.44" x2="66.77" y2="81.66" width="0.254" layer="29"/>
<wire x1="65.66" y1="84.6" x2="67.21" y2="82.98" width="0.254" layer="29"/>
<wire x1="66.64" y1="84.72" x2="68.39" y2="83.42" width="0.254" layer="29"/>
<wire x1="66.43" y1="85.93" x2="68.56" y2="84.88" width="0.254" layer="29"/>
<wire x1="67.25" y1="86.38" x2="69.37" y2="85.7" width="0.254" layer="29"/>
<wire x1="66.71" y1="87.07" x2="69.07" y2="86.91" width="0.254" layer="29"/>
<wire x1="67.37" y1="87.77" x2="69.66" y2="87.74" width="0.254" layer="29"/>
<wire x1="66.67" y1="88.36" x2="69.01" y2="88.68" width="0.254" layer="29"/>
<wire x1="67.09" y1="89.32" x2="69.33" y2="89.74" width="0.254" layer="29"/>
<wire x1="66.23" y1="89.68" x2="68.4" y2="90.65" width="0.254" layer="29"/>
<wire x1="66.34" y1="90.55" x2="68.37" y2="91.64" width="0.254" layer="29"/>
<wire x1="65.39" y1="90.77" x2="67.21" y2="92.2" width="0.254" layer="29"/>
<wire x1="66.73" y1="93.42" x2="65.17" y2="91.64" width="0.254" layer="29"/>
<wire x1="64.2" y1="91.6" x2="65.54" y2="93.51" width="0.254" layer="29"/>
<wire x1="63.7" y1="92.52" x2="64.54" y2="94.54" width="0.254" layer="29"/>
<wire x1="62.76" y1="92.01" x2="63.3" y2="94.29" width="0.254" layer="29"/>
<wire x1="62.2" y1="92.87" x2="62.2" y2="94.99" width="0.254" layer="29"/>
<wire x1="61.62" y1="91.98" x2="61.21" y2="94.45" width="0.254" layer="29"/>
<wire x1="60.88" y1="92.68" x2="60.02" y2="94.65" width="0.254" layer="29"/>
<wire x1="59.35" y1="93.78" x2="60.46" y2="91.8" width="0.254" layer="29"/>
<wire x1="58.16" y1="93.74" x2="59.61" y2="91.86" width="0.254" layer="29"/>
<wire x1="57.76" y1="92.74" x2="59.32" y2="91.02" width="0.254" layer="29"/>
<wire x1="56.68" y1="92.51" x2="58.2" y2="91" width="0.254" layer="29"/>
<wire x1="56.43" y1="91.16" x2="58.29" y2="89.93" width="0.254" layer="29"/>
<wire x1="55.36" y1="90.53" x2="57.42" y2="89.5" width="0.254" layer="29"/>
<wire x1="81.7" y1="92.5" x2="83.2" y2="91" width="0.254" layer="1"/>
<wire x1="83.35" y1="89.88" x2="81.43" y2="91.16" width="0.254" layer="1"/>
<wire x1="80.36" y1="90.54" x2="82.42" y2="89.5" width="0.254" layer="1"/>
<wire x1="79.72" y1="88.01" x2="82.02" y2="87.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="86.89" x2="80.44" y2="86.75" width="0.254" layer="1"/>
<wire x1="80.04" y1="85.52" x2="82.31" y2="86.03" width="0.254" layer="1"/>
<wire x1="83.19" y1="85.42" x2="81.19" y2="84.48" width="0.254" layer="1"/>
<wire x1="81.13" y1="83.25" x2="83.03" y2="84.37" width="0.254" layer="1"/>
<wire x1="84.13" y1="84.13" x2="82.28" y2="82.58" width="0.254" layer="1"/>
<wire x1="83.11" y1="81.33" x2="84.44" y2="83.13" width="0.254" layer="1"/>
<wire x1="85.46" y1="83.28" x2="84.47" y2="81.19" width="0.254" layer="1"/>
<wire x1="85.17" y1="80.42" x2="85.91" y2="82.55" width="0.254" layer="1"/>
<wire x1="86.83" y1="82.93" x2="86.49" y2="80.66" width="0.254" layer="1"/>
<wire x1="87.42" y1="80.06" x2="87.49" y2="82.29" width="0.254" layer="1"/>
<wire x1="88.19" y1="83.04" x2="88.64" y2="80.74" width="0.254" layer="1"/>
<wire x1="89.79" y1="80.69" x2="89.05" y2="82.62" width="0.254" layer="1"/>
<wire x1="89.61" y1="83.6" x2="90.83" y2="81.51" width="0.254" layer="1"/>
<wire x1="91.74" y1="81.7" x2="90.57" y2="83.44" width="0.254" layer="1"/>
<wire x1="90.7" y1="84.56" x2="92.21" y2="82.98" width="0.254" layer="1"/>
<wire x1="93.37" y1="83.44" x2="91.64" y2="84.72" width="0.254" layer="1"/>
<wire x1="91.5" y1="85.9" x2="93.56" y2="84.88" width="0.254" layer="1"/>
<wire x1="94.42" y1="85.68" x2="92.25" y2="86.38" width="0.254" layer="1"/>
<wire x1="91.8" y1="87.07" x2="94.07" y2="86.91" width="0.254" layer="1"/>
<wire x1="94.58" y1="87.74" x2="92.38" y2="87.77" width="0.254" layer="1"/>
<wire x1="91.61" y1="88.36" x2="94.01" y2="88.68" width="0.254" layer="1"/>
<wire x1="94.31" y1="89.74" x2="92.09" y2="89.32" width="0.254" layer="1"/>
<wire x1="91.18" y1="89.66" x2="93.4" y2="90.65" width="0.254" layer="1"/>
<wire x1="93.35" y1="91.62" x2="91.34" y2="90.55" width="0.254" layer="1"/>
<wire x1="90.38" y1="90.76" x2="92.21" y2="92.2" width="0.254" layer="1"/>
<wire x1="91.72" y1="93.41" x2="90.17" y2="91.64" width="0.254" layer="1"/>
<wire x1="89.18" y1="91.59" x2="90.54" y2="93.51" width="0.254" layer="1"/>
<wire x1="89.53" y1="94.53" x2="88.7" y2="92.52" width="0.254" layer="1"/>
<wire x1="87.79" y1="92.12" x2="88.3" y2="94.29" width="0.254" layer="1"/>
<wire x1="84.29" y1="91.05" x2="82.76" y2="92.74" width="0.254" layer="1"/>
<wire x1="83.19" y1="93.7" x2="84.61" y2="91.86" width="0.254" layer="1"/>
<wire x1="85.46" y1="91.8" x2="84.35" y2="93.78" width="0.254" layer="1"/>
<wire x1="85.04" y1="94.61" x2="85.88" y2="92.68" width="0.254" layer="1"/>
<wire x1="86.59" y1="92.07" x2="86.21" y2="94.45" width="0.254" layer="1"/>
<wire x1="87.2" y1="94.96" x2="87.2" y2="92.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="88.5" x2="80.41" y2="89.24" width="0.254" layer="1"/>
<wire x1="80.41" y1="89.24" x2="82.78" y2="88.48" width="0.254" layer="29"/>
<wire x1="79.73" y1="88.01" x2="82.02" y2="87.87" width="0.254" layer="29"/>
<wire x1="80.44" y1="86.75" x2="82.74" y2="86.9" width="0.254" layer="29"/>
<wire x1="82.31" y1="86.03" x2="80.03" y2="85.52" width="0.254" layer="29"/>
<wire x1="81.19" y1="84.48" x2="83.19" y2="85.42" width="0.254" layer="29"/>
<wire x1="81.08" y1="83.22" x2="83.03" y2="84.37" width="0.254" layer="29"/>
<wire x1="84.15" y1="84.16" x2="82.28" y2="82.58" width="0.254" layer="29"/>
<wire x1="84.44" y1="83.13" x2="83.1" y2="81.28" width="0.254" layer="29"/>
<wire x1="84.46" y1="81.19" x2="85.45" y2="83.25" width="0.254" layer="29"/>
<wire x1="85.91" y1="82.55" x2="85.15" y2="80.37" width="0.254" layer="29"/>
<wire x1="86.49" y1="80.66" x2="86.84" y2="82.99" width="0.254" layer="29"/>
<wire x1="87.49" y1="82.29" x2="87.42" y2="80.06" width="0.254" layer="29"/>
<wire x1="88.64" y1="80.74" x2="88.18" y2="83.09" width="0.254" layer="29"/>
<wire x1="89.05" y1="82.62" x2="89.83" y2="80.57" width="0.254" layer="29"/>
<wire x1="90.83" y1="81.52" x2="89.58" y2="83.66" width="0.254" layer="29"/>
<wire x1="90.57" y1="83.44" x2="91.77" y2="81.66" width="0.254" layer="29"/>
<wire x1="90.66" y1="84.6" x2="92.21" y2="82.98" width="0.254" layer="29"/>
<wire x1="91.64" y1="84.72" x2="93.39" y2="83.42" width="0.254" layer="29"/>
<wire x1="91.43" y1="85.93" x2="93.56" y2="84.88" width="0.254" layer="29"/>
<wire x1="92.25" y1="86.38" x2="94.37" y2="85.7" width="0.254" layer="29"/>
<wire x1="91.71" y1="87.07" x2="94.07" y2="86.91" width="0.254" layer="29"/>
<wire x1="92.37" y1="87.77" x2="94.66" y2="87.74" width="0.254" layer="29"/>
<wire x1="91.67" y1="88.36" x2="94.01" y2="88.68" width="0.254" layer="29"/>
<wire x1="92.09" y1="89.32" x2="94.33" y2="89.74" width="0.254" layer="29"/>
<wire x1="91.23" y1="89.68" x2="93.4" y2="90.65" width="0.254" layer="29"/>
<wire x1="91.34" y1="90.55" x2="93.37" y2="91.64" width="0.254" layer="29"/>
<wire x1="90.39" y1="90.77" x2="92.21" y2="92.2" width="0.254" layer="29"/>
<wire x1="91.73" y1="93.42" x2="90.17" y2="91.64" width="0.254" layer="29"/>
<wire x1="89.2" y1="91.6" x2="90.54" y2="93.51" width="0.254" layer="29"/>
<wire x1="88.7" y1="92.52" x2="89.54" y2="94.54" width="0.254" layer="29"/>
<wire x1="87.76" y1="92.01" x2="88.3" y2="94.29" width="0.254" layer="29"/>
<wire x1="87.2" y1="92.87" x2="87.2" y2="94.99" width="0.254" layer="29"/>
<wire x1="86.62" y1="91.98" x2="86.21" y2="94.45" width="0.254" layer="29"/>
<wire x1="85.88" y1="92.68" x2="85.02" y2="94.65" width="0.254" layer="29"/>
<wire x1="84.35" y1="93.78" x2="85.46" y2="91.8" width="0.254" layer="29"/>
<wire x1="83.16" y1="93.74" x2="84.61" y2="91.86" width="0.254" layer="29"/>
<wire x1="82.76" y1="92.74" x2="84.32" y2="91.02" width="0.254" layer="29"/>
<wire x1="81.68" y1="92.51" x2="83.2" y2="91" width="0.254" layer="29"/>
<wire x1="81.43" y1="91.16" x2="83.29" y2="89.93" width="0.254" layer="29"/>
<wire x1="80.36" y1="90.53" x2="82.42" y2="89.5" width="0.254" layer="29"/>
<wire x1="56.7" y1="67.5" x2="58.2" y2="66" width="0.254" layer="1"/>
<wire x1="58.35" y1="64.88" x2="56.43" y2="66.16" width="0.254" layer="1"/>
<wire x1="55.36" y1="65.54" x2="57.42" y2="64.5" width="0.254" layer="1"/>
<wire x1="54.72" y1="63.01" x2="57.02" y2="62.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="61.89" x2="55.44" y2="61.75" width="0.254" layer="1"/>
<wire x1="55.04" y1="60.52" x2="57.31" y2="61.03" width="0.254" layer="1"/>
<wire x1="58.19" y1="60.42" x2="56.19" y2="59.48" width="0.254" layer="1"/>
<wire x1="56.13" y1="58.25" x2="58.03" y2="59.37" width="0.254" layer="1"/>
<wire x1="59.13" y1="59.13" x2="57.28" y2="57.58" width="0.254" layer="1"/>
<wire x1="58.11" y1="56.33" x2="59.44" y2="58.13" width="0.254" layer="1"/>
<wire x1="60.46" y1="58.28" x2="59.47" y2="56.19" width="0.254" layer="1"/>
<wire x1="60.17" y1="55.42" x2="60.91" y2="57.55" width="0.254" layer="1"/>
<wire x1="61.83" y1="57.93" x2="61.49" y2="55.66" width="0.254" layer="1"/>
<wire x1="62.42" y1="55.06" x2="62.49" y2="57.29" width="0.254" layer="1"/>
<wire x1="63.19" y1="58.04" x2="63.64" y2="55.74" width="0.254" layer="1"/>
<wire x1="64.79" y1="55.69" x2="64.05" y2="57.62" width="0.254" layer="1"/>
<wire x1="64.61" y1="58.6" x2="65.83" y2="56.51" width="0.254" layer="1"/>
<wire x1="66.74" y1="56.7" x2="65.57" y2="58.44" width="0.254" layer="1"/>
<wire x1="65.7" y1="59.56" x2="67.21" y2="57.98" width="0.254" layer="1"/>
<wire x1="68.37" y1="58.44" x2="66.64" y2="59.72" width="0.254" layer="1"/>
<wire x1="66.5" y1="60.9" x2="68.56" y2="59.88" width="0.254" layer="1"/>
<wire x1="69.42" y1="60.68" x2="67.25" y2="61.38" width="0.254" layer="1"/>
<wire x1="66.8" y1="62.07" x2="69.07" y2="61.91" width="0.254" layer="1"/>
<wire x1="69.58" y1="62.74" x2="67.38" y2="62.77" width="0.254" layer="1"/>
<wire x1="66.61" y1="63.36" x2="69.01" y2="63.68" width="0.254" layer="1"/>
<wire x1="69.31" y1="64.74" x2="67.09" y2="64.32" width="0.254" layer="1"/>
<wire x1="66.18" y1="64.66" x2="68.4" y2="65.65" width="0.254" layer="1"/>
<wire x1="68.35" y1="66.62" x2="66.34" y2="65.55" width="0.254" layer="1"/>
<wire x1="65.38" y1="65.76" x2="67.21" y2="67.2" width="0.254" layer="1"/>
<wire x1="66.72" y1="68.41" x2="65.17" y2="66.64" width="0.254" layer="1"/>
<wire x1="64.18" y1="66.59" x2="65.54" y2="68.51" width="0.254" layer="1"/>
<wire x1="64.53" y1="69.53" x2="63.7" y2="67.52" width="0.254" layer="1"/>
<wire x1="62.79" y1="67.12" x2="63.3" y2="69.29" width="0.254" layer="1"/>
<wire x1="59.29" y1="66.05" x2="57.76" y2="67.74" width="0.254" layer="1"/>
<wire x1="58.19" y1="68.7" x2="59.61" y2="66.86" width="0.254" layer="1"/>
<wire x1="60.46" y1="66.8" x2="59.35" y2="68.78" width="0.254" layer="1"/>
<wire x1="60.04" y1="69.61" x2="60.88" y2="67.68" width="0.254" layer="1"/>
<wire x1="61.59" y1="67.07" x2="61.21" y2="69.45" width="0.254" layer="1"/>
<wire x1="62.2" y1="69.96" x2="62.2" y2="67.87" width="0.254" layer="1"/>
<wire x1="57.74" y1="63.5" x2="55.41" y2="64.24" width="0.254" layer="1"/>
<wire x1="55.41" y1="64.24" x2="57.78" y2="63.48" width="0.254" layer="29"/>
<wire x1="54.73" y1="63.01" x2="57.02" y2="62.87" width="0.254" layer="29"/>
<wire x1="55.44" y1="61.75" x2="57.74" y2="61.9" width="0.254" layer="29"/>
<wire x1="57.31" y1="61.03" x2="55.03" y2="60.52" width="0.254" layer="29"/>
<wire x1="56.19" y1="59.48" x2="58.19" y2="60.42" width="0.254" layer="29"/>
<wire x1="56.08" y1="58.22" x2="58.03" y2="59.37" width="0.254" layer="29"/>
<wire x1="59.15" y1="59.16" x2="57.28" y2="57.58" width="0.254" layer="29"/>
<wire x1="59.44" y1="58.13" x2="58.1" y2="56.28" width="0.254" layer="29"/>
<wire x1="59.46" y1="56.19" x2="60.45" y2="58.25" width="0.254" layer="29"/>
<wire x1="60.91" y1="57.55" x2="60.15" y2="55.37" width="0.254" layer="29"/>
<wire x1="61.49" y1="55.66" x2="61.84" y2="57.99" width="0.254" layer="29"/>
<wire x1="62.49" y1="57.29" x2="62.42" y2="55.06" width="0.254" layer="29"/>
<wire x1="63.64" y1="55.74" x2="63.18" y2="58.09" width="0.254" layer="29"/>
<wire x1="64.05" y1="57.62" x2="64.83" y2="55.57" width="0.254" layer="29"/>
<wire x1="65.83" y1="56.52" x2="64.58" y2="58.66" width="0.254" layer="29"/>
<wire x1="65.57" y1="58.44" x2="66.77" y2="56.66" width="0.254" layer="29"/>
<wire x1="65.66" y1="59.6" x2="67.21" y2="57.98" width="0.254" layer="29"/>
<wire x1="66.64" y1="59.72" x2="68.39" y2="58.42" width="0.254" layer="29"/>
<wire x1="66.43" y1="60.93" x2="68.56" y2="59.88" width="0.254" layer="29"/>
<wire x1="67.25" y1="61.38" x2="69.37" y2="60.7" width="0.254" layer="29"/>
<wire x1="66.71" y1="62.07" x2="69.07" y2="61.91" width="0.254" layer="29"/>
<wire x1="67.37" y1="62.77" x2="69.66" y2="62.74" width="0.254" layer="29"/>
<wire x1="66.67" y1="63.36" x2="69.01" y2="63.68" width="0.254" layer="29"/>
<wire x1="67.09" y1="64.32" x2="69.33" y2="64.74" width="0.254" layer="29"/>
<wire x1="66.23" y1="64.68" x2="68.4" y2="65.65" width="0.254" layer="29"/>
<wire x1="66.34" y1="65.55" x2="68.37" y2="66.64" width="0.254" layer="29"/>
<wire x1="65.39" y1="65.77" x2="67.21" y2="67.2" width="0.254" layer="29"/>
<wire x1="66.73" y1="68.42" x2="65.17" y2="66.64" width="0.254" layer="29"/>
<wire x1="64.2" y1="66.6" x2="65.54" y2="68.51" width="0.254" layer="29"/>
<wire x1="63.7" y1="67.52" x2="64.54" y2="69.54" width="0.254" layer="29"/>
<wire x1="62.76" y1="67.01" x2="63.3" y2="69.29" width="0.254" layer="29"/>
<wire x1="62.2" y1="67.87" x2="62.2" y2="69.99" width="0.254" layer="29"/>
<wire x1="61.62" y1="66.98" x2="61.21" y2="69.45" width="0.254" layer="29"/>
<wire x1="60.88" y1="67.68" x2="60.02" y2="69.65" width="0.254" layer="29"/>
<wire x1="59.35" y1="68.78" x2="60.46" y2="66.8" width="0.254" layer="29"/>
<wire x1="58.16" y1="68.74" x2="59.61" y2="66.86" width="0.254" layer="29"/>
<wire x1="57.76" y1="67.74" x2="59.32" y2="66.02" width="0.254" layer="29"/>
<wire x1="56.68" y1="67.51" x2="58.2" y2="66" width="0.254" layer="29"/>
<wire x1="56.43" y1="66.16" x2="58.29" y2="64.93" width="0.254" layer="29"/>
<wire x1="55.36" y1="65.53" x2="57.42" y2="64.5" width="0.254" layer="29"/>
<wire x1="81.7" y1="67.5" x2="83.2" y2="66" width="0.254" layer="1"/>
<wire x1="83.35" y1="64.88" x2="81.43" y2="66.16" width="0.254" layer="1"/>
<wire x1="80.36" y1="65.54" x2="82.42" y2="64.5" width="0.254" layer="1"/>
<wire x1="79.72" y1="63.01" x2="82.02" y2="62.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="61.89" x2="80.44" y2="61.75" width="0.254" layer="1"/>
<wire x1="80.04" y1="60.52" x2="82.31" y2="61.03" width="0.254" layer="1"/>
<wire x1="83.19" y1="60.42" x2="81.19" y2="59.48" width="0.254" layer="1"/>
<wire x1="81.13" y1="58.25" x2="83.03" y2="59.37" width="0.254" layer="1"/>
<wire x1="84.13" y1="59.13" x2="82.28" y2="57.58" width="0.254" layer="1"/>
<wire x1="83.11" y1="56.33" x2="84.44" y2="58.13" width="0.254" layer="1"/>
<wire x1="85.46" y1="58.28" x2="84.47" y2="56.19" width="0.254" layer="1"/>
<wire x1="85.17" y1="55.42" x2="85.91" y2="57.55" width="0.254" layer="1"/>
<wire x1="86.83" y1="57.93" x2="86.49" y2="55.66" width="0.254" layer="1"/>
<wire x1="87.42" y1="55.06" x2="87.49" y2="57.29" width="0.254" layer="1"/>
<wire x1="88.19" y1="58.04" x2="88.64" y2="55.74" width="0.254" layer="1"/>
<wire x1="89.79" y1="55.69" x2="89.05" y2="57.62" width="0.254" layer="1"/>
<wire x1="89.61" y1="58.6" x2="90.83" y2="56.51" width="0.254" layer="1"/>
<wire x1="91.74" y1="56.7" x2="90.57" y2="58.44" width="0.254" layer="1"/>
<wire x1="90.7" y1="59.56" x2="92.21" y2="57.98" width="0.254" layer="1"/>
<wire x1="93.37" y1="58.44" x2="91.64" y2="59.72" width="0.254" layer="1"/>
<wire x1="91.5" y1="60.9" x2="93.56" y2="59.88" width="0.254" layer="1"/>
<wire x1="94.42" y1="60.68" x2="92.25" y2="61.38" width="0.254" layer="1"/>
<wire x1="91.8" y1="62.07" x2="94.07" y2="61.91" width="0.254" layer="1"/>
<wire x1="94.58" y1="62.74" x2="92.38" y2="62.77" width="0.254" layer="1"/>
<wire x1="91.61" y1="63.36" x2="94.01" y2="63.68" width="0.254" layer="1"/>
<wire x1="94.31" y1="64.74" x2="92.09" y2="64.32" width="0.254" layer="1"/>
<wire x1="91.18" y1="64.66" x2="93.4" y2="65.65" width="0.254" layer="1"/>
<wire x1="93.35" y1="66.62" x2="91.34" y2="65.55" width="0.254" layer="1"/>
<wire x1="90.38" y1="65.76" x2="92.21" y2="67.2" width="0.254" layer="1"/>
<wire x1="91.72" y1="68.41" x2="90.17" y2="66.64" width="0.254" layer="1"/>
<wire x1="89.18" y1="66.59" x2="90.54" y2="68.51" width="0.254" layer="1"/>
<wire x1="89.53" y1="69.53" x2="88.7" y2="67.52" width="0.254" layer="1"/>
<wire x1="87.79" y1="67.12" x2="88.3" y2="69.29" width="0.254" layer="1"/>
<wire x1="84.29" y1="66.05" x2="82.76" y2="67.74" width="0.254" layer="1"/>
<wire x1="83.19" y1="68.7" x2="84.61" y2="66.86" width="0.254" layer="1"/>
<wire x1="85.46" y1="66.8" x2="84.35" y2="68.78" width="0.254" layer="1"/>
<wire x1="85.04" y1="69.61" x2="85.88" y2="67.68" width="0.254" layer="1"/>
<wire x1="86.59" y1="67.07" x2="86.21" y2="69.45" width="0.254" layer="1"/>
<wire x1="87.2" y1="69.96" x2="87.2" y2="67.87" width="0.254" layer="1"/>
<wire x1="82.74" y1="63.5" x2="80.41" y2="64.24" width="0.254" layer="1"/>
<wire x1="80.41" y1="64.24" x2="82.78" y2="63.48" width="0.254" layer="29"/>
<wire x1="79.73" y1="63.01" x2="82.02" y2="62.87" width="0.254" layer="29"/>
<wire x1="80.44" y1="61.75" x2="82.74" y2="61.9" width="0.254" layer="29"/>
<wire x1="82.31" y1="61.03" x2="80.03" y2="60.52" width="0.254" layer="29"/>
<wire x1="81.19" y1="59.48" x2="83.19" y2="60.42" width="0.254" layer="29"/>
<wire x1="81.08" y1="58.22" x2="83.03" y2="59.37" width="0.254" layer="29"/>
<wire x1="84.15" y1="59.16" x2="82.28" y2="57.58" width="0.254" layer="29"/>
<wire x1="84.44" y1="58.13" x2="83.1" y2="56.28" width="0.254" layer="29"/>
<wire x1="84.46" y1="56.19" x2="85.45" y2="58.25" width="0.254" layer="29"/>
<wire x1="85.91" y1="57.55" x2="85.15" y2="55.37" width="0.254" layer="29"/>
<wire x1="86.49" y1="55.66" x2="86.84" y2="57.99" width="0.254" layer="29"/>
<wire x1="87.49" y1="57.29" x2="87.42" y2="55.06" width="0.254" layer="29"/>
<wire x1="88.64" y1="55.74" x2="88.18" y2="58.09" width="0.254" layer="29"/>
<wire x1="89.05" y1="57.62" x2="89.83" y2="55.57" width="0.254" layer="29"/>
<wire x1="90.83" y1="56.52" x2="89.58" y2="58.66" width="0.254" layer="29"/>
<wire x1="90.57" y1="58.44" x2="91.77" y2="56.66" width="0.254" layer="29"/>
<wire x1="90.66" y1="59.6" x2="92.21" y2="57.98" width="0.254" layer="29"/>
<wire x1="91.64" y1="59.72" x2="93.39" y2="58.42" width="0.254" layer="29"/>
<wire x1="91.43" y1="60.93" x2="93.56" y2="59.88" width="0.254" layer="29"/>
<wire x1="92.25" y1="61.38" x2="94.37" y2="60.7" width="0.254" layer="29"/>
<wire x1="91.71" y1="62.07" x2="94.07" y2="61.91" width="0.254" layer="29"/>
<wire x1="92.37" y1="62.77" x2="94.66" y2="62.74" width="0.254" layer="29"/>
<wire x1="91.67" y1="63.36" x2="94.01" y2="63.68" width="0.254" layer="29"/>
<wire x1="92.09" y1="64.32" x2="94.33" y2="64.74" width="0.254" layer="29"/>
<wire x1="91.23" y1="64.68" x2="93.4" y2="65.65" width="0.254" layer="29"/>
<wire x1="91.34" y1="65.55" x2="93.37" y2="66.64" width="0.254" layer="29"/>
<wire x1="90.39" y1="65.77" x2="92.21" y2="67.2" width="0.254" layer="29"/>
<wire x1="91.73" y1="68.42" x2="90.17" y2="66.64" width="0.254" layer="29"/>
<wire x1="89.2" y1="66.6" x2="90.54" y2="68.51" width="0.254" layer="29"/>
<wire x1="88.7" y1="67.52" x2="89.54" y2="69.54" width="0.254" layer="29"/>
<wire x1="87.76" y1="67.01" x2="88.3" y2="69.29" width="0.254" layer="29"/>
<wire x1="87.2" y1="67.87" x2="87.2" y2="69.99" width="0.254" layer="29"/>
<wire x1="86.62" y1="66.98" x2="86.21" y2="69.45" width="0.254" layer="29"/>
<wire x1="85.88" y1="67.68" x2="85.02" y2="69.65" width="0.254" layer="29"/>
<wire x1="84.35" y1="68.78" x2="85.46" y2="66.8" width="0.254" layer="29"/>
<wire x1="83.16" y1="68.74" x2="84.61" y2="66.86" width="0.254" layer="29"/>
<wire x1="82.76" y1="67.74" x2="84.32" y2="66.02" width="0.254" layer="29"/>
<wire x1="81.68" y1="67.51" x2="83.2" y2="66" width="0.254" layer="29"/>
<wire x1="81.43" y1="66.16" x2="83.29" y2="64.93" width="0.254" layer="29"/>
<wire x1="80.36" y1="65.53" x2="82.42" y2="64.5" width="0.254" layer="29"/>
<wire x1="0" y1="0" x2="0" y2="99.9998" width="0.0254" layer="51"/>
<wire x1="0" y1="0" x2="99.9998" y2="0" width="0.0254" layer="51"/>
<wire x1="49.5046" y1="49.5046" x2="50.4952" y2="50.4952" width="0.0254" layer="51"/>
<wire x1="49.5046" y1="50.4952" x2="50.4952" y2="49.5046" width="0.0254" layer="51"/>
<wire x1="99.9998" y1="0" x2="99.9998" y2="99.9998" width="0.0254" layer="51"/>
<wire x1="99.9998" y1="99.9998" x2="0" y2="99.9998" width="0.0254" layer="51"/>
<circle x="12.2" y="37.5" radius="7.5" width="0.254" layer="1"/>
<circle x="12.21" y="37.51" radius="4.5" width="0.254" layer="1"/>
<circle x="12.2" y="37.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="12.2" y="37.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="37.2" y="37.5" radius="7.5" width="0.254" layer="1"/>
<circle x="37.21" y="37.51" radius="4.5" width="0.254" layer="1"/>
<circle x="37.2" y="37.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="37.2" y="37.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="12.2" y="12.5" radius="7.5" width="0.254" layer="1"/>
<circle x="12.21" y="12.51" radius="4.5" width="0.254" layer="1"/>
<circle x="12.2" y="12.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="12.2" y="12.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="37.2" y="12.5" radius="7.5" width="0.254" layer="1"/>
<circle x="37.21" y="12.51" radius="4.5" width="0.254" layer="1"/>
<circle x="37.2" y="12.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="37.2" y="12.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="62.2" y="37.5" radius="7.5" width="0.254" layer="1"/>
<circle x="62.21" y="37.51" radius="4.5" width="0.254" layer="1"/>
<circle x="62.2" y="37.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="62.2" y="37.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="87.2" y="37.5" radius="7.5" width="0.254" layer="1"/>
<circle x="87.21" y="37.51" radius="4.5" width="0.254" layer="1"/>
<circle x="87.2" y="37.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="87.2" y="37.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="62.2" y="12.5" radius="7.5" width="0.254" layer="1"/>
<circle x="62.21" y="12.51" radius="4.5" width="0.254" layer="1"/>
<circle x="62.2" y="12.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="62.2" y="12.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="87.2" y="12.5" radius="7.5" width="0.254" layer="1"/>
<circle x="87.21" y="12.51" radius="4.5" width="0.254" layer="1"/>
<circle x="87.2" y="12.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="87.2" y="12.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="12.2" y="87.5" radius="7.5" width="0.254" layer="1"/>
<circle x="12.21" y="87.51" radius="4.5" width="0.254" layer="1"/>
<circle x="12.2" y="87.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="12.2" y="87.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="37.2" y="87.5" radius="7.5" width="0.254" layer="1"/>
<circle x="37.21" y="87.51" radius="4.5" width="0.254" layer="1"/>
<circle x="37.2" y="87.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="37.2" y="87.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="12.2" y="62.5" radius="7.5" width="0.254" layer="1"/>
<circle x="12.21" y="62.51" radius="4.5" width="0.254" layer="1"/>
<circle x="12.2" y="62.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="12.2" y="62.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="37.2" y="62.5" radius="7.5" width="0.254" layer="1"/>
<circle x="37.21" y="62.51" radius="4.5" width="0.254" layer="1"/>
<circle x="37.2" y="62.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="37.2" y="62.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="62.2" y="87.5" radius="7.5" width="0.254" layer="1"/>
<circle x="62.21" y="87.51" radius="4.5" width="0.254" layer="1"/>
<circle x="62.2" y="87.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="62.2" y="87.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="87.2" y="87.5" radius="7.5" width="0.254" layer="1"/>
<circle x="87.21" y="87.51" radius="4.5" width="0.254" layer="1"/>
<circle x="87.2" y="87.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="87.2" y="87.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="62.2" y="62.5" radius="7.5" width="0.254" layer="1"/>
<circle x="62.21" y="62.51" radius="4.5" width="0.254" layer="1"/>
<circle x="62.2" y="62.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="62.2" y="62.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="87.2" y="62.5" radius="7.5" width="0.254" layer="1"/>
<circle x="87.21" y="62.51" radius="4.5" width="0.254" layer="1"/>
<circle x="87.2" y="62.51" radius="4.5106" width="0.254" layer="29"/>
<circle x="87.2" y="62.5" radius="7.4998" width="0.254" layer="29"/>
<circle x="2.5146" y="2.4892" radius="1.27" width="0.0254" layer="51"/>
<circle x="2.5146" y="47.498" radius="1.2446" width="0.0254" layer="51"/>
<circle x="47.498" y="2.4892" radius="1.27" width="0.0254" layer="51"/>
<circle x="47.498" y="47.498" radius="1.2446" width="0.0254" layer="51"/>
<circle x="2.5146" y="52.5018" radius="1.2446" width="0.0254" layer="51"/>
<circle x="2.5146" y="97.5106" radius="1.2446" width="0.0254" layer="51"/>
<circle x="47.498" y="52.5018" radius="1.2446" width="0.0254" layer="51"/>
<circle x="47.498" y="97.5106" radius="1.2446" width="0.0254" layer="51"/>
<circle x="52.5018" y="2.4892" radius="1.27" width="0.0254" layer="51"/>
<circle x="52.5018" y="47.498" radius="1.2446" width="0.0254" layer="51"/>
<circle x="97.5106" y="2.4892" radius="1.27" width="0.0254" layer="51"/>
<circle x="97.5106" y="47.498" radius="1.2446" width="0.0254" layer="51"/>
<circle x="52.5018" y="52.5018" radius="1.2446" width="0.0254" layer="51"/>
<circle x="52.5018" y="97.5106" radius="1.2446" width="0.0254" layer="51"/>
<circle x="97.5106" y="52.5018" radius="1.2446" width="0.0254" layer="51"/>
<circle x="97.5106" y="97.5106" radius="1.2446" width="0.0254" layer="51"/>
<pad name="BTN9-2" x="7.828" y="38.481" drill="0.6" stop="no"/>
<pad name="BTN9-1" x="5.288" y="40.513" drill="0.6" stop="no"/>
<pad name="BTN10-2" x="32.828" y="38.481" drill="0.6" stop="no"/>
<pad name="BTN10-1" x="30.288" y="40.513" drill="0.6" stop="no"/>
<pad name="BTN13-2" x="7.828" y="13.481" drill="0.6" stop="no"/>
<pad name="BTN13-1" x="5.288" y="15.513" drill="0.6" stop="no"/>
<pad name="BTN14-2" x="32.828" y="13.481" drill="0.6" stop="no"/>
<pad name="BTN14-1" x="30.288" y="15.513" drill="0.6" stop="no"/>
<pad name="BTN11-2" x="57.828" y="38.481" drill="0.6" stop="no"/>
<pad name="BTN11-1" x="55.288" y="40.513" drill="0.6" stop="no"/>
<pad name="BTN12-2" x="82.828" y="38.481" drill="0.6" stop="no"/>
<pad name="BTN12-1" x="80.288" y="40.513" drill="0.6" stop="no"/>
<pad name="BTN15-2" x="57.828" y="13.481" drill="0.6" stop="no"/>
<pad name="BTN15-1" x="55.288" y="15.513" drill="0.6" stop="no"/>
<pad name="BTN16-2" x="82.828" y="13.481" drill="0.6" stop="no"/>
<pad name="BTN16-1" x="80.288" y="15.513" drill="0.6" stop="no"/>
<pad name="BTN1-2" x="7.828" y="88.481" drill="0.508" stop="no"/>
<pad name="BTN1-1" x="5.288" y="90.513" drill="0.508" stop="no"/>
<pad name="BTN2-2" x="32.828" y="88.481" drill="0.508" stop="no"/>
<pad name="BTN2-1" x="30.288" y="90.513" drill="0.508" stop="no"/>
<pad name="BTN5-2" x="7.828" y="63.481" drill="0.6" stop="no"/>
<pad name="BTN5-1" x="5.288" y="65.513" drill="0.6" stop="no"/>
<pad name="BTN6-2" x="32.828" y="63.481" drill="0.6" stop="no"/>
<pad name="BTN6-1" x="30.288" y="65.513" drill="0.6" stop="no"/>
<pad name="BTN3-2" x="57.828" y="88.481" drill="0.508" stop="no"/>
<pad name="BTN3-1" x="55.288" y="90.513" drill="0.508" stop="no"/>
<pad name="BTN4-2" x="82.828" y="88.481" drill="0.508" stop="no"/>
<pad name="BTN4-1" x="80.288" y="90.513" drill="0.508" stop="no"/>
<pad name="BTN7-2" x="57.828" y="63.481" drill="0.6" stop="no"/>
<pad name="BTN7-1" x="55.288" y="65.513" drill="0.6" stop="no"/>
<pad name="BTN8-2" x="82.828" y="63.481" drill="0.6" stop="no"/>
<pad name="BTN8-1" x="80.288" y="65.513" drill="0.6" stop="no"/>
<text x="11.75" y="97.5" size="1.016" layer="21">Col0</text>
<text x="36.75" y="97.5" size="1.016" layer="21">Col1</text>
<text x="61.75" y="97.5" size="1.016" layer="21">Col2</text>
<text x="85.75" y="97.5" size="1.016" layer="21">Col3</text>
<text x="3" y="86.25" size="1.016" layer="21" rot="R90">Row0</text>
<text x="2.75" y="61.25" size="1.016" layer="21" rot="R90">Row1</text>
<text x="2.75" y="36.25" size="1.016" layer="21" rot="R90">Row2</text>
<text x="2.75" y="11" size="1.016" layer="21" rot="R90">Row3</text>
<hole x="12.5" y="25" drill="3"/>
<hole x="25" y="37.5" drill="3"/>
<hole x="37.5" y="25" drill="3"/>
<hole x="25" y="12.5" drill="3"/>
<hole x="62.5" y="25" drill="3"/>
<hole x="75" y="37.5" drill="3"/>
<hole x="87.5" y="25" drill="3"/>
<hole x="75" y="12.5" drill="3"/>
<hole x="12.5" y="75" drill="3"/>
<hole x="25" y="87.5" drill="3"/>
<hole x="37.5" y="75" drill="3"/>
<hole x="25" y="62.5" drill="3"/>
<hole x="62.5" y="75" drill="3"/>
<hole x="75" y="87.5" drill="3"/>
<hole x="87.5" y="75" drill="3"/>
<hole x="75" y="62.5" drill="3"/>
<hole x="2.484" y="97.516" drill="3"/>
<hole x="97.516" y="97.516" drill="3"/>
<hole x="97.516" y="2.484" drill="3"/>
<hole x="2.484" y="2.484" drill="3"/>
</package>
</packages>
<symbols>
<symbol name="WS2812">
<pin name="VDD" x="5.08" y="15.24" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="DI" x="-12.7" y="-2.54" visible="pad" length="middle" direction="in"/>
<pin name="GND" x="0" y="-10.16" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="DO" x="12.7" y="-2.54" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="6.35" x2="-5.08" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-6.35" y2="6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-2.54" y2="8.89" width="0.254" layer="94"/>
<wire x1="-2.54" y1="8.89" x2="-5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="-5.08" y1="8.89" x2="-5.08" y2="6.35" width="0.254" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="8.89" width="0.254" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="-2.54" y2="8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="8.89" x2="-1.27" y2="8.89" width="0.254" layer="94"/>
<wire x1="-5.08" y1="6.35" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-7.62" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="5.08" y1="-1.27" x2="7.62" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="-1.27" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="3.81" y="1.27" size="1.27" layer="94" rot="R180">WS2812</text>
<wire x1="-3.81" y1="3.81" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="BUTTONPAD_SINGLE">
<wire x1="10.16" y1="7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="8.382" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.27" layer="95">&gt;VALUE</text>
<pin name="SIG2" x="12.7" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="SIG1" x="-10.16" y="5.08" visible="off" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WS2812" prefix="LED">
<gates>
<gate name="G$1" symbol="WS2812" x="0" y="-2.54"/>
</gates>
<devices>
<device name="GUN" package="WS2812">
<connects>
<connect gate="G$1" pin="DI" pad="DIN"/>
<connect gate="G$1" pin="DO" pad="DOUT"/>
<connect gate="G$1" pin="GND" pad="VSS"/>
<connect gate="G$1" pin="VDD" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUTTONPAD-4X4">
<description>Proto layout and schematic for 4x4 button pad. Spark Fun Electronics SKU : COM-07835. Compatible common cathode tri-color LED SKU : COM-00105</description>
<gates>
<gate name="BTN1" symbol="BUTTONPAD_SINGLE" x="0" y="0"/>
<gate name="BTN2" symbol="BUTTONPAD_SINGLE" x="35.56" y="0"/>
<gate name="BTN3" symbol="BUTTONPAD_SINGLE" x="71.12" y="0"/>
<gate name="BTN4" symbol="BUTTONPAD_SINGLE" x="106.68" y="0"/>
<gate name="BTN5" symbol="BUTTONPAD_SINGLE" x="0" y="-22.86"/>
<gate name="BTN6" symbol="BUTTONPAD_SINGLE" x="35.56" y="-22.86"/>
<gate name="BTN7" symbol="BUTTONPAD_SINGLE" x="71.12" y="-22.86"/>
<gate name="BTN8" symbol="BUTTONPAD_SINGLE" x="106.68" y="-22.86"/>
<gate name="BTN9" symbol="BUTTONPAD_SINGLE" x="0" y="-45.72"/>
<gate name="BTN10" symbol="BUTTONPAD_SINGLE" x="35.56" y="-45.72"/>
<gate name="BTN11" symbol="BUTTONPAD_SINGLE" x="71.12" y="-45.72"/>
<gate name="BTN12" symbol="BUTTONPAD_SINGLE" x="106.68" y="-45.72"/>
<gate name="BTN13" symbol="BUTTONPAD_SINGLE" x="0" y="-68.58"/>
<gate name="BTN14" symbol="BUTTONPAD_SINGLE" x="35.56" y="-68.58"/>
<gate name="BTN15" symbol="BUTTONPAD_SINGLE" x="71.12" y="-68.58"/>
<gate name="BTN16" symbol="BUTTONPAD_SINGLE" x="106.68" y="-68.58"/>
</gates>
<devices>
<device name="GUNFINGER4X4" package="BUTTONPAD-4X4">
<connects>
<connect gate="BTN1" pin="SIG1" pad="BTN1-1"/>
<connect gate="BTN1" pin="SIG2" pad="BTN1-2"/>
<connect gate="BTN10" pin="SIG1" pad="BTN10-1"/>
<connect gate="BTN10" pin="SIG2" pad="BTN10-2"/>
<connect gate="BTN11" pin="SIG1" pad="BTN11-1"/>
<connect gate="BTN11" pin="SIG2" pad="BTN11-2"/>
<connect gate="BTN12" pin="SIG1" pad="BTN12-1"/>
<connect gate="BTN12" pin="SIG2" pad="BTN12-2"/>
<connect gate="BTN13" pin="SIG1" pad="BTN13-1"/>
<connect gate="BTN13" pin="SIG2" pad="BTN13-2"/>
<connect gate="BTN14" pin="SIG1" pad="BTN14-1"/>
<connect gate="BTN14" pin="SIG2" pad="BTN14-2"/>
<connect gate="BTN15" pin="SIG1" pad="BTN15-1"/>
<connect gate="BTN15" pin="SIG2" pad="BTN15-2"/>
<connect gate="BTN16" pin="SIG1" pad="BTN16-1"/>
<connect gate="BTN16" pin="SIG2" pad="BTN16-2"/>
<connect gate="BTN2" pin="SIG1" pad="BTN2-1"/>
<connect gate="BTN2" pin="SIG2" pad="BTN2-2"/>
<connect gate="BTN3" pin="SIG1" pad="BTN3-1"/>
<connect gate="BTN3" pin="SIG2" pad="BTN3-2"/>
<connect gate="BTN4" pin="SIG1" pad="BTN4-1"/>
<connect gate="BTN4" pin="SIG2" pad="BTN4-2"/>
<connect gate="BTN5" pin="SIG1" pad="BTN5-1"/>
<connect gate="BTN5" pin="SIG2" pad="BTN5-2"/>
<connect gate="BTN6" pin="SIG1" pad="BTN6-1"/>
<connect gate="BTN6" pin="SIG2" pad="BTN6-2"/>
<connect gate="BTN7" pin="SIG1" pad="BTN7-1"/>
<connect gate="BTN7" pin="SIG2" pad="BTN7-2"/>
<connect gate="BTN8" pin="SIG1" pad="BTN8-1"/>
<connect gate="BTN8" pin="SIG2" pad="BTN8-2"/>
<connect gate="BTN9" pin="SIG1" pad="BTN9-1"/>
<connect gate="BTN9" pin="SIG2" pad="BTN9-2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X04">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<text x="2.286" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.286" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="12.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="3.4" x2="12.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="12.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.25" y1="3.15" x2="12.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="3.15" x2="12.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="2.15" x2="12.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.016" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4_LOCK">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="2.667" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-3.556" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_MALE">
<description>&lt;h3&gt;SMD - 4 Pin Right Angle Male Header&lt;/h3&gt;
tDocu layer shows pin locations.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
<text x="-4.318" y="6.731" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-4-PTH">
<description>&lt;h3&gt;JST Right Angle 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="3.81" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.81" y="1.524" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1MM_RA">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_VERTICAL_COMBO">
<description>&lt;h3&gt;SMD - 4 Pin Vertical Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;SMD Pad count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-0.508" y="2.921" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_LONG">
<description>&lt;h3&gt;SMD - 4 Pin w/ Long Solder Pads&lt;/h3&gt;
No silk, but tDocu layer shows pin position. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<description>&lt;h3&gt;JST Vertical 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.jst-mfg.com/product/pdf/eng/ePH.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
<text x="-1.143" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 4 Pin Right-Angle Female Header&lt;/h3&gt;
Silk outline shows header location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_04">
<description>&lt;h3&gt;4 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_04" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="CONN_04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="2xCONN-08399" constant="no"/>
<attribute name="SF_ID" value="2xPRT-08084" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="1X04_1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X04_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-13531" constant="no"/>
<attribute name="SF_ID" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="1X04_1MM_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10310" constant="no"/>
<attribute name="SF_ID" value="PRT-10208" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_VERTICAL_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04_SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13251"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10KOHM" prefix="R">
<description>&lt;h3&gt;10kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/6W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/6W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/6W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00824"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="LED1" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED2" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED3" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED4" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED5" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED6" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED7" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED8" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED9" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED10" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED11" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED12" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED13" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED14" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED15" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="LED16" library="OmaticEagleParts" deviceset="WS2812" device="GUN"/>
<part name="U$1" library="OmaticEagleParts" deviceset="BUTTONPAD-4X4" device="GUNFINGER4X4"/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="J4" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R3" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R4" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R6" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R7" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R8" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R9" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R10" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R11" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R12" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C4" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C7" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C9" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C10" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C11" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C12" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C14" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C15" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="C16" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" value="0.1uF"/>
<part name="R13" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R14" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R15" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="R16" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" value="10k"/>
<part name="J5" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LED1" gate="G$1" x="17.78" y="139.7"/>
<instance part="LED2" gate="G$1" x="53.34" y="139.7"/>
<instance part="LED3" gate="G$1" x="86.36" y="139.7"/>
<instance part="LED4" gate="G$1" x="121.92" y="139.7"/>
<instance part="LED5" gate="G$1" x="157.48" y="139.7"/>
<instance part="LED6" gate="G$1" x="190.5" y="139.7"/>
<instance part="LED7" gate="G$1" x="223.52" y="139.7"/>
<instance part="LED8" gate="G$1" x="259.08" y="139.7"/>
<instance part="LED9" gate="G$1" x="292.1" y="139.7"/>
<instance part="LED10" gate="G$1" x="325.12" y="139.7"/>
<instance part="LED11" gate="G$1" x="358.14" y="139.7"/>
<instance part="LED12" gate="G$1" x="393.7" y="139.7"/>
<instance part="LED13" gate="G$1" x="429.26" y="139.7"/>
<instance part="LED14" gate="G$1" x="464.82" y="139.7"/>
<instance part="LED15" gate="G$1" x="497.84" y="139.7"/>
<instance part="LED16" gate="G$1" x="530.86" y="139.7"/>
<instance part="U$1" gate="BTN1" x="22.86" y="53.34"/>
<instance part="U$1" gate="BTN2" x="106.68" y="53.34"/>
<instance part="U$1" gate="BTN3" x="185.42" y="53.34"/>
<instance part="U$1" gate="BTN4" x="287.02" y="48.26"/>
<instance part="U$1" gate="BTN5" x="22.86" y="17.78"/>
<instance part="U$1" gate="BTN6" x="106.68" y="17.78"/>
<instance part="U$1" gate="BTN7" x="187.96" y="12.7"/>
<instance part="U$1" gate="BTN8" x="287.02" y="15.24"/>
<instance part="U$1" gate="BTN9" x="22.86" y="-17.78"/>
<instance part="U$1" gate="BTN10" x="106.68" y="-17.78"/>
<instance part="U$1" gate="BTN11" x="187.96" y="-22.86"/>
<instance part="U$1" gate="BTN12" x="287.02" y="-17.78"/>
<instance part="U$1" gate="BTN13" x="22.86" y="-50.8"/>
<instance part="U$1" gate="BTN14" x="106.68" y="-50.8"/>
<instance part="U$1" gate="BTN15" x="187.96" y="-50.8"/>
<instance part="U$1" gate="BTN16" x="289.56" y="-53.34"/>
<instance part="J2" gate="G$1" x="10.16" y="101.6" rot="R180"/>
<instance part="J1" gate="G$1" x="45.72" y="101.6" rot="R180"/>
<instance part="J3" gate="G$1" x="81.28" y="101.6" rot="R180"/>
<instance part="J4" gate="G$1" x="116.84" y="101.6" rot="R180"/>
<instance part="R1" gate="G$1" x="-2.54" y="58.42"/>
<instance part="R2" gate="G$1" x="2.54" y="22.86"/>
<instance part="R3" gate="G$1" x="2.54" y="-12.7"/>
<instance part="R4" gate="G$1" x="-2.54" y="-45.72"/>
<instance part="R5" gate="G$1" x="81.28" y="-45.72"/>
<instance part="R6" gate="G$1" x="83.82" y="-12.7"/>
<instance part="R7" gate="G$1" x="83.82" y="22.86"/>
<instance part="R8" gate="G$1" x="162.56" y="58.42"/>
<instance part="R9" gate="G$1" x="165.1" y="17.78"/>
<instance part="R10" gate="G$1" x="165.1" y="-17.78"/>
<instance part="R11" gate="G$1" x="165.1" y="-45.72"/>
<instance part="R12" gate="G$1" x="83.82" y="58.42"/>
<instance part="C1" gate="G$1" x="20.32" y="160.02" rot="R90"/>
<instance part="C2" gate="G$1" x="533.4" y="157.48" rot="R90"/>
<instance part="C3" gate="G$1" x="500.38" y="157.48" rot="R90"/>
<instance part="C4" gate="G$1" x="467.36" y="157.48" rot="R90"/>
<instance part="C5" gate="G$1" x="431.8" y="157.48" rot="R90"/>
<instance part="C6" gate="G$1" x="396.24" y="157.48" rot="R90"/>
<instance part="C7" gate="G$1" x="360.68" y="157.48" rot="R90"/>
<instance part="C8" gate="G$1" x="327.66" y="157.48" rot="R90"/>
<instance part="C9" gate="G$1" x="294.64" y="157.48" rot="R90"/>
<instance part="C10" gate="G$1" x="261.62" y="157.48" rot="R90"/>
<instance part="C11" gate="G$1" x="226.06" y="157.48" rot="R90"/>
<instance part="C12" gate="G$1" x="193.04" y="157.48" rot="R90"/>
<instance part="C13" gate="G$1" x="160.02" y="157.48" rot="R90"/>
<instance part="C14" gate="G$1" x="124.46" y="160.02" rot="R90"/>
<instance part="C15" gate="G$1" x="88.9" y="160.02" rot="R90"/>
<instance part="C16" gate="G$1" x="55.88" y="160.02" rot="R90"/>
<instance part="R13" gate="G$1" x="261.62" y="53.34"/>
<instance part="R14" gate="G$1" x="261.62" y="20.32"/>
<instance part="R15" gate="G$1" x="264.16" y="-12.7"/>
<instance part="R16" gate="G$1" x="266.7" y="-48.26"/>
<instance part="J5" gate="G$1" x="147.32" y="101.6" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="DO"/>
<pinref part="LED2" gate="G$1" pin="DI"/>
<wire x1="30.48" y1="137.16" x2="40.64" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="DO"/>
<pinref part="LED3" gate="G$1" pin="DI"/>
<wire x1="66.04" y1="137.16" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="DO"/>
<pinref part="LED4" gate="G$1" pin="DI"/>
<wire x1="99.06" y1="137.16" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="DO"/>
<pinref part="LED5" gate="G$1" pin="DI"/>
<wire x1="134.62" y1="137.16" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LED5" gate="G$1" pin="DO"/>
<pinref part="LED6" gate="G$1" pin="DI"/>
<wire x1="170.18" y1="137.16" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="DO"/>
<pinref part="LED7" gate="G$1" pin="DI"/>
<wire x1="203.2" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED7" gate="G$1" pin="DO"/>
<pinref part="LED8" gate="G$1" pin="DI"/>
<wire x1="236.22" y1="137.16" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED8" gate="G$1" pin="DO"/>
<pinref part="LED9" gate="G$1" pin="DI"/>
<wire x1="271.78" y1="137.16" x2="279.4" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED9" gate="G$1" pin="DO"/>
<wire x1="304.8" y1="137.16" x2="312.42" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LED10" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="LED10" gate="G$1" pin="DO"/>
<pinref part="LED11" gate="G$1" pin="DI"/>
<wire x1="337.82" y1="137.16" x2="345.44" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LED11" gate="G$1" pin="DO"/>
<wire x1="370.84" y1="137.16" x2="381" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LED12" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="LED12" gate="G$1" pin="DO"/>
<pinref part="LED13" gate="G$1" pin="DI"/>
<wire x1="406.4" y1="137.16" x2="416.56" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="LED13" gate="G$1" pin="DO"/>
<pinref part="LED14" gate="G$1" pin="DI"/>
<wire x1="441.96" y1="137.16" x2="452.12" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LED14" gate="G$1" pin="DO"/>
<pinref part="LED15" gate="G$1" pin="DI"/>
<wire x1="477.52" y1="137.16" x2="485.14" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="LED15" gate="G$1" pin="DO"/>
<pinref part="LED16" gate="G$1" pin="DI"/>
<wire x1="510.54" y1="137.16" x2="518.16" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTN2" class="0">
<segment>
<pinref part="U$1" gate="BTN2" pin="SIG1"/>
<wire x1="88.9" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="93.98" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="93.98" y1="58.42" x2="93.98" y2="63.5" width="0.1524" layer="91"/>
<junction x="93.98" y="58.42"/>
<label x="93.98" y="63.5" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="5.08" y1="101.6" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<label x="2.54" y="101.6" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN1" class="0">
<segment>
<pinref part="U$1" gate="BTN1" pin="SIG1"/>
<wire x1="12.7" y1="58.42" x2="5.08" y2="58.42" width="0.1524" layer="91"/>
<wire x1="2.54" y1="58.42" x2="5.08" y2="58.42" width="0.1524" layer="91"/>
<wire x1="5.08" y1="58.42" x2="5.08" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<label x="5.08" y="63.5" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="5.08" y1="104.14" x2="2.54" y2="104.14" width="0.1524" layer="91"/>
<label x="2.54" y="104.14" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="BTN1" pin="SIG2"/>
<wire x1="35.56" y1="58.42" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<label x="45.72" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN5" pin="SIG2"/>
<wire x1="35.56" y1="22.86" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
<label x="43.18" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN9" pin="SIG2"/>
<wire x1="35.56" y1="-12.7" x2="43.18" y2="-12.7" width="0.1524" layer="91"/>
<label x="43.18" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN13" pin="SIG2"/>
<wire x1="35.56" y1="-45.72" x2="43.18" y2="-45.72" width="0.1524" layer="91"/>
<label x="43.18" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN2" pin="SIG2"/>
<wire x1="119.38" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<label x="129.54" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN6" pin="SIG2"/>
<wire x1="119.38" y1="22.86" x2="127" y2="22.86" width="0.1524" layer="91"/>
<label x="127" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN10" pin="SIG2"/>
<wire x1="119.38" y1="-12.7" x2="127" y2="-12.7" width="0.1524" layer="91"/>
<label x="127" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN14" pin="SIG2"/>
<wire x1="119.38" y1="-45.72" x2="127" y2="-45.72" width="0.1524" layer="91"/>
<label x="127" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN3" pin="SIG2"/>
<wire x1="198.12" y1="58.42" x2="205.74" y2="58.42" width="0.1524" layer="91"/>
<label x="205.74" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN7" pin="SIG2"/>
<wire x1="200.66" y1="17.78" x2="205.74" y2="17.78" width="0.1524" layer="91"/>
<label x="205.74" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN11" pin="SIG2"/>
<wire x1="200.66" y1="-17.78" x2="208.28" y2="-17.78" width="0.1524" layer="91"/>
<label x="208.28" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN15" pin="SIG2"/>
<wire x1="200.66" y1="-45.72" x2="208.28" y2="-45.72" width="0.1524" layer="91"/>
<label x="208.28" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN4" pin="SIG2"/>
<wire x1="299.72" y1="53.34" x2="304.8" y2="53.34" width="0.1524" layer="91"/>
<label x="304.8" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN8" pin="SIG2"/>
<wire x1="299.72" y1="20.32" x2="307.34" y2="20.32" width="0.1524" layer="91"/>
<label x="307.34" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN12" pin="SIG2"/>
<wire x1="299.72" y1="-12.7" x2="307.34" y2="-12.7" width="0.1524" layer="91"/>
<label x="307.34" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN16" pin="SIG2"/>
<wire x1="302.26" y1="-48.26" x2="309.88" y2="-48.26" width="0.1524" layer="91"/>
<label x="309.88" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="160.02" x2="10.16" y2="160.02" width="0.1524" layer="91"/>
<label x="10.16" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="50.8" y1="160.02" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<label x="48.26" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="83.82" y1="160.02" x2="81.28" y2="160.02" width="0.1524" layer="91"/>
<label x="81.28" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="119.38" y1="160.02" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
<label x="116.84" y="160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="154.94" y1="157.48" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
<label x="152.4" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="187.96" y1="157.48" x2="185.42" y2="157.48" width="0.1524" layer="91"/>
<label x="185.42" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="220.98" y1="157.48" x2="215.9" y2="157.48" width="0.1524" layer="91"/>
<label x="215.9" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="256.54" y1="157.48" x2="251.46" y2="157.48" width="0.1524" layer="91"/>
<label x="251.46" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="289.56" y1="157.48" x2="284.48" y2="157.48" width="0.1524" layer="91"/>
<label x="284.48" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="322.58" y1="157.48" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<label x="320.04" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="355.6" y1="157.48" x2="353.06" y2="157.48" width="0.1524" layer="91"/>
<label x="353.06" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="391.16" y1="157.48" x2="388.62" y2="157.48" width="0.1524" layer="91"/>
<label x="388.62" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="426.72" y1="157.48" x2="421.64" y2="157.48" width="0.1524" layer="91"/>
<label x="421.64" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="462.28" y1="157.48" x2="459.74" y2="157.48" width="0.1524" layer="91"/>
<label x="459.74" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="495.3" y1="157.48" x2="492.76" y2="157.48" width="0.1524" layer="91"/>
<label x="492.76" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="528.32" y1="157.48" x2="525.78" y2="157.48" width="0.1524" layer="91"/>
<label x="525.78" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="142.24" y1="99.06" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<label x="139.7" y="99.06" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN5" class="0">
<segment>
<pinref part="U$1" gate="BTN5" pin="SIG1"/>
<wire x1="12.7" y1="22.86" x2="10.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="10.16" y1="22.86" x2="7.62" y2="22.86" width="0.1524" layer="91"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="27.94" width="0.1524" layer="91"/>
<junction x="10.16" y="22.86"/>
<label x="10.16" y="27.94" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="104.14" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
<label x="38.1" y="104.14" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN9" class="0">
<segment>
<pinref part="U$1" gate="BTN9" pin="SIG1"/>
<wire x1="12.7" y1="-12.7" x2="10.16" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-12.7" x2="7.62" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<junction x="10.16" y="-12.7"/>
<label x="10.16" y="-5.08" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="76.2" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<label x="73.66" y="104.14" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN13" class="0">
<segment>
<pinref part="U$1" gate="BTN13" pin="SIG1"/>
<wire x1="12.7" y1="-45.72" x2="7.62" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="7.62" y1="-45.72" x2="2.54" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-45.72" x2="7.62" y2="-40.64" width="0.1524" layer="91"/>
<junction x="7.62" y="-45.72"/>
<label x="7.62" y="-40.64" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="111.76" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<label x="109.22" y="104.14" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN6" class="0">
<segment>
<pinref part="U$1" gate="BTN6" pin="SIG1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="96.52" y1="22.86" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<wire x1="93.98" y1="22.86" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
<wire x1="93.98" y1="22.86" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<junction x="93.98" y="22.86"/>
<label x="93.98" y="27.94" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="40.64" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="38.1" y="101.6" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN10" class="0">
<segment>
<pinref part="U$1" gate="BTN10" pin="SIG1"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-12.7" x2="91.44" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-12.7" x2="88.9" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-12.7" x2="91.44" y2="-7.62" width="0.1524" layer="91"/>
<junction x="91.44" y="-12.7"/>
<label x="91.44" y="-7.62" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="76.2" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<label x="73.66" y="101.6" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN14" class="0">
<segment>
<pinref part="U$1" gate="BTN14" pin="SIG1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-45.72" x2="91.44" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-45.72" x2="86.36" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-45.72" x2="91.44" y2="-40.64" width="0.1524" layer="91"/>
<junction x="91.44" y="-45.72"/>
<label x="91.44" y="-40.64" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="111.76" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<label x="109.22" y="101.6" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN3" class="0">
<segment>
<pinref part="U$1" gate="BTN3" pin="SIG1"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="175.26" y1="58.42" x2="170.18" y2="58.42" width="0.1524" layer="91"/>
<wire x1="170.18" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="63.5" width="0.1524" layer="91"/>
<junction x="170.18" y="58.42"/>
<label x="170.18" y="63.5" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="5.08" y1="99.06" x2="2.54" y2="99.06" width="0.1524" layer="91"/>
<label x="2.54" y="99.06" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN7" class="0">
<segment>
<pinref part="U$1" gate="BTN7" pin="SIG1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="177.8" y1="17.78" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="172.72" y1="17.78" x2="170.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="172.72" y1="17.78" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
<junction x="172.72" y="17.78"/>
<label x="172.72" y="22.86" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="40.64" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<label x="38.1" y="99.06" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN11" class="0">
<segment>
<pinref part="U$1" gate="BTN11" pin="SIG1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="177.8" y1="-17.78" x2="172.72" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-17.78" x2="170.18" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-17.78" x2="172.72" y2="-12.7" width="0.1524" layer="91"/>
<junction x="172.72" y="-17.78"/>
<label x="172.72" y="-12.7" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="76.2" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<label x="73.66" y="99.06" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN15" class="0">
<segment>
<pinref part="U$1" gate="BTN15" pin="SIG1"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="177.8" y1="-45.72" x2="172.72" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-45.72" x2="170.18" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-45.72" x2="172.72" y2="-40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="-45.72"/>
<label x="172.72" y="-40.64" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="111.76" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<label x="109.22" y="99.06" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN4" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="5.08" y1="96.52" x2="2.54" y2="96.52" width="0.1524" layer="91"/>
<label x="2.54" y="96.52" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN4" pin="SIG1"/>
<wire x1="276.86" y1="53.34" x2="271.78" y2="53.34" width="0.1524" layer="91"/>
<wire x1="271.78" y1="53.34" x2="266.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="271.78" y1="53.34" x2="271.78" y2="58.42" width="0.1524" layer="91"/>
<junction x="271.78" y="53.34"/>
<pinref part="R13" gate="G$1" pin="2"/>
<label x="271.78" y="58.42" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BTN8" class="0">
<segment>
<pinref part="U$1" gate="BTN8" pin="SIG1"/>
<wire x1="276.86" y1="20.32" x2="271.78" y2="20.32" width="0.1524" layer="91"/>
<wire x1="271.78" y1="20.32" x2="266.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="271.78" y1="20.32" x2="271.78" y2="25.4" width="0.1524" layer="91"/>
<junction x="271.78" y="20.32"/>
<pinref part="R14" gate="G$1" pin="2"/>
<label x="271.78" y="25.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="40.64" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="38.1" y="96.52" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BTN12" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<wire x1="76.2" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<label x="73.66" y="96.52" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN12" pin="SIG1"/>
<wire x1="276.86" y1="-12.7" x2="274.32" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="274.32" y1="-12.7" x2="269.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="274.32" y1="-12.7" x2="274.32" y2="-7.62" width="0.1524" layer="91"/>
<junction x="274.32" y="-12.7"/>
<pinref part="R15" gate="G$1" pin="2"/>
<label x="274.32" y="-7.62" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BTN16" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="111.76" y1="96.52" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<label x="109.22" y="96.52" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="BTN16" pin="SIG1"/>
<wire x1="279.4" y1="-48.26" x2="274.32" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="274.32" y1="-48.26" x2="271.78" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="274.32" y1="-48.26" x2="274.32" y2="-43.18" width="0.1524" layer="91"/>
<junction x="274.32" y="-48.26"/>
<pinref part="R16" gate="G$1" pin="2"/>
<label x="274.32" y="-43.18" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LEDIN" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="142.24" y1="104.14" x2="139.7" y2="104.14" width="0.1524" layer="91"/>
<label x="139.7" y="104.14" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="DI"/>
<wire x1="5.08" y1="137.16" x2="0" y2="137.16" width="0.1524" layer="91"/>
<label x="0" y="137.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LEDOUT" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="142.24" y1="101.6" x2="139.7" y2="101.6" width="0.1524" layer="91"/>
<label x="139.7" y="101.6" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="LED16" gate="G$1" pin="DO"/>
<wire x1="543.56" y1="137.16" x2="551.18" y2="137.16" width="0.1524" layer="91"/>
<label x="551.18" y="137.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="142.24" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<label x="139.7" y="96.52" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="157.48" y1="58.42" x2="152.4" y2="58.42" width="0.1524" layer="91"/>
<label x="152.4" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="58.42" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
<label x="-12.7" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-45.72" x2="-12.7" y2="-45.72" width="0.1524" layer="91"/>
<label x="-12.7" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="-12.7" x2="-10.16" y2="-12.7" width="0.1524" layer="91"/>
<label x="-10.16" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="22.86" x2="-10.16" y2="22.86" width="0.1524" layer="91"/>
<label x="-10.16" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="256.54" y1="53.34" x2="254" y2="53.34" width="0.1524" layer="91"/>
<label x="254" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="78.74" y1="22.86" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<label x="71.12" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="160.02" y1="17.78" x2="154.94" y2="17.78" width="0.1524" layer="91"/>
<label x="154.94" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="256.54" y1="20.32" x2="254" y2="20.32" width="0.1524" layer="91"/>
<label x="254" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="160.02" y1="-17.78" x2="154.94" y2="-17.78" width="0.1524" layer="91"/>
<label x="154.94" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-12.7" x2="71.12" y2="-12.7" width="0.1524" layer="91"/>
<label x="71.12" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-45.72" x2="71.12" y2="-45.72" width="0.1524" layer="91"/>
<label x="71.12" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="160.02" y1="-45.72" x2="154.94" y2="-45.72" width="0.1524" layer="91"/>
<label x="157.48" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="259.08" y1="-12.7" x2="256.54" y2="-12.7" width="0.1524" layer="91"/>
<label x="256.54" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="261.62" y1="-48.26" x2="259.08" y2="-48.26" width="0.1524" layer="91"/>
<label x="259.08" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="VDD"/>
<wire x1="22.86" y1="154.94" x2="22.86" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="VDD"/>
<wire x1="58.42" y1="154.94" x2="58.42" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="VDD"/>
<wire x1="91.44" y1="154.94" x2="91.44" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="VDD"/>
<wire x1="127" y1="154.94" x2="127" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED5" gate="G$1" pin="VDD"/>
<wire x1="162.56" y1="154.94" x2="162.56" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="VDD"/>
<wire x1="195.58" y1="154.94" x2="195.58" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED7" gate="G$1" pin="VDD"/>
<wire x1="228.6" y1="154.94" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED8" gate="G$1" pin="VDD"/>
<wire x1="264.16" y1="154.94" x2="264.16" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED9" gate="G$1" pin="VDD"/>
<wire x1="297.18" y1="154.94" x2="297.18" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED10" gate="G$1" pin="VDD"/>
<wire x1="330.2" y1="154.94" x2="330.2" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED11" gate="G$1" pin="VDD"/>
<wire x1="363.22" y1="154.94" x2="363.22" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED12" gate="G$1" pin="VDD"/>
<wire x1="398.78" y1="154.94" x2="398.78" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED13" gate="G$1" pin="VDD"/>
<wire x1="434.34" y1="154.94" x2="434.34" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED14" gate="G$1" pin="VDD"/>
<wire x1="469.9" y1="154.94" x2="469.9" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED15" gate="G$1" pin="VDD"/>
<wire x1="502.92" y1="154.94" x2="502.92" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED16" gate="G$1" pin="VDD"/>
<wire x1="535.94" y1="154.94" x2="535.94" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="78.74" y1="58.42" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<label x="71.12" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
